open_project                ../../../project/fpga_audio_processor.xpr
generate_target simulation  [get_files ../../../ip/fixed_to_float/fixed_to_float.xci]
generate_target simulation  [get_files ../../../ip/fp_absolute_value/fp_absolute_value.xci]
generate_target simulation  [get_files ../../../ip/fp_greater_comp/fp_greater_comp.xci]
generate_target simulation  [get_files ../../../ip/fp_subtractor/fp_subtractor.xci]
generate_target simulation  [get_files ../../../ip/fp_multiplier/fp_multiplier.xci]
generate_target simulation  [get_files ../../../ip/fp_adder/fp_adder.xci]
generate_target simulation  [get_files ../../../ip/fp_divider/fp_divider.xci]
generate_target simulation  [get_files ../../../ip/lookahead_fifo/lookahead_fifo.xci]
close_project
exec xvlog                  ../../../ip/fixed_to_float/sim/fixed_to_float.v
exec xvlog                  ../../../ip/fp_absolute_value/sim/fp_absolute_value.v
exec xvlog                  ../../../ip/fp_greater_comp/sim/fp_greater_comp.v
exec xvlog                  ../../../ip/fp_subtractor/sim/fp_subtractor.v
exec xvlog                  ../../../ip/fp_multiplier/sim/fp_multiplier.v
exec xvlog                  ../../../ip/fp_adder/sim/fp_adder.v
exec xvlog                  ../../../ip/fp_divider/sim/fp_divider.v
exec xvlog                  ../../../ip/lookahead_fifo/sim/lookahead_fifo.v
exec xvlog -sv              ../../../hdl/limiter_fsm.sv
exec xvlog -sv              ../../../hdl/limiter.sv
exec xvlog -sv              ../../common/wave_file_reader.sv
exec xvlog -sv              ./limiter_tc_01.sv
exec xelab                  limiter_tc_01 -s limiter_tc_01_sim -debug typical -L floating_point_v7_1_13 -L fifo_generator_v13_2_6 -L unisims_ver -L unimacro_ver
exec xsim                   limiter_tc_01_sim -t xsim.tcl
start_gui
open_wave_database          limiter_tc_01_sim.wdb