open_project                ../../../project/fpga_audio_processor.xpr
generate_target simulation  [get_files ../../../ip/fixed_to_float/fixed_to_float.xci]
generate_target simulation  [get_files ../../../ip/single_to_double/single_to_double.xci]
generate_target simulation  [get_files ../../../ip/dp_fp_multiplier/dp_fp_multiplier.xci]
generate_target simulation  [get_files ../../../ip/dp_fp_adder/dp_fp_adder.xci]
generate_target simulation  [get_files ../../../ip/fir_filter_coeff_drom/fir_filter_coeff_drom.xci]
generate_target simulation  [get_files ../../../ip/fir_filter_sample_dram/fir_filter_sample_dram.xci]
generate_target simulation  [get_files ../../../ip/fp_mult_add/fp_mult_add.xci]
generate_target simulation  [get_files ../../../ip/double_to_single/double_to_single.xci]
close_project
exec xvlog                  ../../../ip/fixed_to_float/sim/fixed_to_float.v
exec xvlog                  ../../../ip/single_to_double/sim/single_to_double.v
exec xvlog                  ../../../ip/dp_fp_multiplier/sim/dp_fp_multiplier.v
exec xvlog                  ../../../ip/dp_fp_adder/sim/dp_fp_adder.v
exec xvlog                  ../../../ip/fir_filter_coeff_drom/sim/fir_filter_coeff_drom.v
exec xvlog                  ../../../ip/fir_filter_sample_dram/sim/fir_filter_sample_dram.v
exec xvlog                  ../../../ip/fp_mult_add/sim/fp_mult_add.v
exec xvlog                  ../../../ip/double_to_single/sim/double_to_single.v
exec xvlog -sv              ../../../hdl/single_to_double_converter.sv
exec xvlog -sv              ../../../hdl/biquad_equation.sv
exec xvlog -sv              ../../../hdl/biquad_filter.sv
exec xvlog -sv              ../../../hdl/double_to_single_converter.sv
exec xvlog -sv              ../../../hdl/fir_filter.sv
exec xvlog -sv              ../../../hdl/equalizer.sv
exec xvlog -sv              ../../common/wave_file_reader.sv
exec xvlog -sv              ./equalizer_tc_01.sv
exec xelab                  equalizer_tc_01 -s equalizer_tc_01_sim -debug typical -L floating_point_v7_1_13 -L dist_mem_gen_v8_0_13 -L unisims_ver -L unimacro_ver
exec xsim                   equalizer_tc_01_sim -t xsim.tcl
start_gui
open_wave_database          equalizer_tc_01_sim.wdb