exec xvlog -sv      ../../../hdl/led_meter_fsm.sv
exec xvlog -sv      ../../../hdl/led_meter.sv
exec xvlog -sv      ../../common/wave_file_reader.sv
exec xvlog -sv      ./led_meter_tc_01.sv
exec xelab          led_meter_tc_01 -s led_meter_tc_01_sim -debug typical
exec xsim           led_meter_tc_01_sim -t xsim.tcl
start_gui
open_wave_database  led_meter_tc_01_sim.wdb