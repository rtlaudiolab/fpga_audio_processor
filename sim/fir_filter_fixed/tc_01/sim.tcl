open_project                ../../../ip/managed_ip_project/managed_ip_project.xpr
generate_target simulation  [get_files ../../../ip/fixed_point_multiplier/fixed_point_multiplier.xci]
generate_target simulation  [get_files ../../../ip/fir_filter_fixed_coeff_drom/fir_filter_fixed_coeff_drom.xci]
generate_target simulation  [get_files ../../../ip/fir_filter_fixed_sample_dram/fir_filter_fixed_sample_dram.xci]
close_project
exec xvhdl                  ../../../ip/fixed_point_multiplier/sim/fixed_point_multiplier.vhd
exec xvlog                  ../../../ip/fir_filter_fixed_coeff_drom/sim/fir_filter_fixed_coeff_drom.v
exec xvlog                  ../../../ip/fir_filter_fixed_sample_dram/sim/fir_filter_fixed_sample_dram.v
exec xvlog -sv              ../../../hdl/fir_filter_fixed_fsm.sv
exec xvlog -sv              ../../../hdl/fir_filter_fixed.sv
exec xvlog -sv              ../../common/wave_file_reader.sv
exec xvlog -sv              ./fir_filter_fixed_tc_01.sv
exec xelab                  fir_filter_fixed_tc_01 -s fir_filter_fixed_tc_01_sim -debug typical -L dist_mem_gen_v8_0_13 -L unisims_ver -L unimacro_ver
exec xsim                   fir_filter_fixed_tc_01_sim -t xsim.tcl
start_gui
open_wave_database          fir_filter_fixed_tc_01_sim.wdb