module clipper_wrapper (
    input   wire               i_clock,
    // Audio Input
    input   wire               i_data_valid,
    input   wire   [31 : 0]    i_data_left,
    input   wire   [31 : 0]    i_data_right,
    // Audio Output
    output  wire               o_data_valid,
    output  wire   [31 : 0]    o_data_left,
    output  wire   [31 : 0]    o_data_right
);

    clipper clipper_inst (
        .i_clock        (i_clock),
        .i_data_valid   (i_data_valid),
        .i_data_left    (i_data_left),
        .i_data_right   (i_data_right),
        .o_data_valid   (o_data_valid),
        .o_data_left    (o_data_left),
        .o_data_right   (o_data_right)
    );

endmodule
