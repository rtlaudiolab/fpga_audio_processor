module led_meter_wrapper (
    input   wire i_clock,
    // Audio Input
    input   wire signed [23 : 0]   i_data_left,
    input   wire signed [23 : 0]   i_data_right,
    input   wire                   i_data_valid,
    // LED Meter Output
    output  wire [7 : 0]   o_led
);

    led_meter led_meter_inst (
        .i_clock        (i_clock),
        .i_data_left    (i_data_left),
        .i_data_right   (i_data_right),
        .i_data_valid   (i_data_valid),
        .o_led          (o_led)
    );

endmodule
