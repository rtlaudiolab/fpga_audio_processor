module clock_generator (
    input   logic i_clock,
    output  logic o_clock_100MHz,
    output  logic o_clock_45MHz
);

    timeunit 1ns;
    timeprecision 1ps;

    // Clock Generator IP core for the Audio Codec
    logic clock_45MHZ;     // 44.1 KHz * 1024 = 45.169664 MHz, core generates 45.16765 MHz

    clk_wiz_0 clk_wiz_0_inst (
        .i_clock    (i_clock),
        .o_clock_1  (o_clock_100MHz),
        .o_clock_2  (clock_45MHZ)
    );

    // ODDR Primitive for the Output Clock
    oddr_0 oddr_0_inst (
        .clk_in     (clock_45MHZ),
        .clk_out    (o_clock_45MHz)
    );

endmodule
