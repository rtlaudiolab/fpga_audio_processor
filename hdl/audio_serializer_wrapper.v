module audio_serializer_wrapper (
    input   wire            i_clock,
    // I2S Interface
    input   wire            i_codec_bit_clock,
    input   wire            i_codec_lr_clock,
    output  wire            o_codec_dac_data,
    // Parallel Data Input
    input   wire [23 : 0]   i_data_left,
    input   wire [23 : 0]   i_data_right,
    input   wire            i_data_valid
);

    audio_serializer audio_serializer_inst (
        .i_clock             (i_clock),
        .i_codec_bit_clock   (i_codec_bit_clock),
        .i_codec_lr_clock    (i_codec_lr_clock),
        .o_codec_dac_data    (o_codec_dac_data),
        .i_data_left         (i_data_left),
        .i_data_right        (i_data_right),
        .i_data_valid        (i_data_valid)
    );

endmodule
