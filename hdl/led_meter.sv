module led_meter (
    input   logic i_clock,
    // Audio Input
    input   logic signed [23 : 0]   i_data_left,
    input   logic signed [23 : 0]   i_data_right,
    input   logic                   i_data_valid,
    // LED Meter Output
    output  logic [7 : 0]   o_led
);

    timeunit 1ns;
    timeprecision 1ps;

    led_meter_fsm led_meter_fsm_left_inst (
    .i_clock        (i_clock),
    // Audio Input
    .i_data         (i_data_left),
    .i_data_valid   (i_data_valid),
    // LED Meter Output
    .o_led          (o_led[7 : 4])
    );

    logic [3 : 0] aux;
    led_meter_fsm led_meter_fsm_right_inst (
    .i_clock        (i_clock),
    // Audio Input
    .i_data         (i_data_right),
    .i_data_valid   (i_data_valid),
    // LED Meter Output
    .o_led          (aux)
    );

    // Maps the right channel LEDs so the highest sample value is shown on the right
    assign o_led[3] = aux[0];
    assign o_led[2] = aux[1];
    assign o_led[1] = aux[2];
    assign o_led[0] = aux[3];

endmodule
