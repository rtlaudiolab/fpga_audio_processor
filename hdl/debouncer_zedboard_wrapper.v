module debouncer_zedboard_wrapper # (
    parameter integer SWITCH_COUNT              = 8,
    parameter integer BUTTON_COUNT              = 5,
    parameter integer DEBOUNCE_COUNTER_WIDTH    = 16
) (
    // Clock
    input   wire i_clock,
    // Debounce counter values
    input   wire [DEBOUNCE_COUNTER_WIDTH-1 : 0] i_switch_debounce_counter,
    input   wire [DEBOUNCE_COUNTER_WIDTH-1 : 0] i_button_debounce_counter,
    // Input switches
    input   wire i_sw0,
    input   wire i_sw1,
    input   wire i_sw2,
    input   wire i_sw3,
    input   wire i_sw4,
    input   wire i_sw5,
    input   wire i_sw6,
    input   wire i_sw7,
    // Input buttons
    input   wire i_btnu,
    input   wire i_btnd,
    input   wire i_btnl,
    input   wire i_btnr,
    input   wire i_btnc,
    // Debounced switch outputs
    output  wire o_sw0,
    output  wire o_sw1,
    output  wire o_sw2,
    output  wire o_sw3,
    output  wire o_sw4,
    output  wire o_sw5,
    output  wire o_sw6,
    output  wire o_sw7,
    // Debounced button outputs
    output  wire o_btnu,
    output  wire o_btnd,
    output  wire o_btnl,
    output  wire o_btnr,
    output  wire o_btnc
);

    debouncer_zedboard # (
        .SWITCH_COUNT           (SWITCH_COUNT),
        .BUTTON_COUNT           (BUTTON_COUNT),
        .DEBOUNCE_COUNTER_WIDTH (DEBOUNCE_COUNTER_WIDTH)
    ) debouncer_zedboard_inst (
        .i_clock                    (i_clock),
        .i_switch_debounce_counter  (i_switch_debounce_counter),
        .i_button_debounce_counter  (i_button_debounce_counter),
        .i_sw0                      (i_sw0),
        .i_sw1                      (i_sw1),
        .i_sw2                      (i_sw2),
        .i_sw3                      (i_sw3),
        .i_sw4                      (i_sw4),
        .i_sw5                      (i_sw5),
        .i_sw6                      (i_sw6),
        .i_sw7                      (i_sw7),
        .i_btnu                     (i_btnu),
        .i_btnd                     (i_btnd),
        .i_btnl                     (i_btnl),
        .i_btnr                     (i_btnr),
        .i_btnc                     (i_btnc),
        .o_sw0                      (o_sw0),
        .o_sw1                      (o_sw1),
        .o_sw2                      (o_sw2),
        .o_sw3                      (o_sw3),
        .o_sw4                      (o_sw4),
        .o_sw5                      (o_sw5),
        .o_sw6                      (o_sw6),
        .o_sw7                      (o_sw7),
        .o_btnu                     (o_btnu),
        .o_btnd                     (o_btnd),
        .o_btnl                     (o_btnl),
        .o_btnr                     (o_btnr),
        .o_btnc                     (o_btnc)
    );

endmodule
