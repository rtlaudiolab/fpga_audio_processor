module fir_filter_fsm # (
    parameter integer SP_FLOATING_POINT_BIT_WIDTH = 32
    ) (
    input   logic                                       i_clock,
    // Audio Input
    input   logic                                       i_data_valid,
    input   logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_data,
    // Audio Output
    output  logic                                       o_data_valid,
    output  logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] o_data
);

    timeunit 1ns;
    timeprecision 1ps;

    logic [4 : 0]   sample_dram_wr_addr;
    logic [4 : 0]   sample_dram_rd_addr;
    logic [31 : 0]  sample_dram_data_out;
    fir_filter_sample_dram fir_filter_sample_dram_inst (
        .a          (sample_dram_wr_addr),
        .d          (i_data),
        .dpra       (sample_dram_rd_addr),
        .clk        (i_clock),
        .we         (i_data_valid),
        .qdpo       (sample_dram_data_out)
    );

    logic [4 : 0]   coeff_drom_rd_addr;
    logic [31 : 0]  coeff_drom_data_out;
    fir_filter_coeff_drom fir_filter_coeff_drom_inst (
        .a          (coeff_drom_rd_addr),
        .clk        (i_clock),
        .qspo       (coeff_drom_data_out)
    );

    logic                                       fp_mult_add_data_in_valid;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_mult_add_data_in_a;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_mult_add_data_in_b;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_mult_add_data_in_c;
    logic                                       fp_mult_add_data_out_valid;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_mult_add_data_out;
    fp_mult_add fp_mult_add_inst (
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (fp_mult_add_data_in_valid),
        .s_axis_a_tdata         (fp_mult_add_data_in_a),
        .s_axis_b_tvalid        (fp_mult_add_data_in_valid),
        .s_axis_b_tdata         (fp_mult_add_data_in_b),
        .s_axis_c_tvalid        (fp_mult_add_data_in_valid),
        .s_axis_c_tdata         (fp_mult_add_data_in_c),
        .m_axis_result_tvalid   (fp_mult_add_data_out_valid),
        .m_axis_result_tdata    (fp_mult_add_data_out)
    );

    // FIR Filter FSM
    typedef enum logic [2 : 0]  {IDLE,
                                WAIT_SAMPLE,
                                START_MAC,
                                WAIT_FOR_MAC,
                                UPDATE_OUTPUT} fir_filter_fsm_t;
    fir_filter_fsm_t fir_filter_fsm_state = IDLE;
    logic [4 : 0] mac_counter;
    logic sample_buffer_full;
    logic mac_busy;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] accumulator;

    always_ff @(posedge i_clock) begin : fir_filter_fsm
        case (fir_filter_fsm_state)

            IDLE : begin
                fp_mult_add_data_in_a <= 32'd0;
                fp_mult_add_data_in_b <= 32'd0;
                fp_mult_add_data_in_c <= 32'd0;
                fp_mult_add_data_in_valid <= 1'b1;
                sample_dram_wr_addr <= 5'd0;
                sample_dram_rd_addr <= 5'b0;
                coeff_drom_rd_addr <= 5'b0;
                mac_counter <= 5'd0;
                sample_buffer_full <= 1'b0;
                mac_busy <= 1'b0;
                o_data_valid <= 1'b0;
                o_data <= 32'd0;
                fir_filter_fsm_state <= WAIT_SAMPLE;
            end

            WAIT_SAMPLE : begin
                o_data_valid <= 1'b0;
                fp_mult_add_data_in_valid <= 1'b0;
                if (sample_buffer_full == 1'b0) begin
                    if (i_data_valid == 1'b1) begin
                        sample_dram_wr_addr <= sample_dram_wr_addr + 1;
                        mac_counter <=  mac_counter + 1;
                    end
                    if (mac_counter == 5'd30) begin
                        sample_buffer_full <= 1'b1;
                    end
                end else begin
                    if (i_data_valid == 1'b1) begin
                        sample_dram_wr_addr <= sample_dram_wr_addr + 1;
                        sample_dram_rd_addr <= sample_dram_wr_addr + 1;
                        coeff_drom_rd_addr <= 5'd0;
                        mac_counter <= 5'd0;
                        fir_filter_fsm_state <= START_MAC;
                    end
                end
            end

            START_MAC : begin
                if (mac_counter == 5'd30) begin
                    mac_counter <= 5'd0;
                    fir_filter_fsm_state <= UPDATE_OUTPUT;
                end else begin
                    fp_mult_add_data_in_valid <= 1'b1;
                    fp_mult_add_data_in_a <= sample_dram_data_out;
                    fp_mult_add_data_in_b <= coeff_drom_data_out;
                    fp_mult_add_data_in_c <= accumulator;
                    fir_filter_fsm_state <= WAIT_FOR_MAC;
                end
            end

            WAIT_FOR_MAC : begin
                fp_mult_add_data_in_valid <= 1'b0;
                if (fp_mult_add_data_out_valid == 1'b1) begin
                    mac_counter <= mac_counter + 1;
                    accumulator <= fp_mult_add_data_out;
                    sample_dram_rd_addr <= sample_dram_rd_addr + 1;
                    coeff_drom_rd_addr <= coeff_drom_rd_addr + 1;
                    fir_filter_fsm_state <= START_MAC;
                end
            end

            UPDATE_OUTPUT : begin
                accumulator <= 32'd0;
                o_data_valid <= 1'b1;
                o_data <= accumulator;
                fir_filter_fsm_state <= WAIT_SAMPLE;
            end

            default : begin
                fir_filter_fsm_state <= IDLE;
            end
        endcase
    end

endmodule
