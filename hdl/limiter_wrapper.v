module limiter_wrapper # (
    parameter integer SP_FLOATING_POINT_BIT_WIDTH = 32
    ) (
    input   wire                                           i_clock,
    input   wire                                           i_enable,
    // Audio Input
    input   wire                                           i_data_valid,
    input   wire   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]     i_data_left,
    input   wire   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]     i_data_right,
    // Controls
    input   wire   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]     i_linear_threshold,
    // Audio Output
    output  wire                                           o_data_valid,
    output  wire   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]     o_data_left,
    output  wire   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]     o_data_right
);

limiter #(
    .SP_FLOATING_POINT_BIT_WIDTH (SP_FLOATING_POINT_BIT_WIDTH)
) limiter_inst (
  .i_clock              (i_clock),
  .i_enable             (i_enable),
  .i_data_valid         (i_data_valid),
  .i_data_left          (i_data_left),
  .i_data_right         (i_data_right),
  .i_linear_threshold   (i_linear_threshold),
  .o_data_valid         (o_data_valid),
  .o_data_left          (o_data_left),
  .o_data_right         (o_data_right)
);

endmodule
