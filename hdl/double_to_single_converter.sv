module double_to_single_converter # (
    parameter integer SP_FLOATING_POINT_BIT_WIDTH = 32,
    parameter integer DP_FLOATING_POINT_BIT_WIDTH = 64
    ) (
    input   logic                                       i_clock,
    // Audio Input
    input   logic                                       i_data_valid,
    input   logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_data_left,
    input   logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_data_right,
    // Audio Output
    output  logic                                       o_data_valid,
    output  logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] o_data_left,
    output  logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] o_data_right
);

    timeunit 1ns;
    timeprecision 1ps;

    // Single-to-double Conversion
    logic                                       double_to_single_valid_in;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   double_to_single_data_in;
    logic                                       double_to_single_valid_out;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   double_to_single_data_out;
    double_to_single double_to_single_inst (
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (double_to_single_valid_in),
        .s_axis_a_tdata         (double_to_single_data_in),
        .m_axis_result_tvalid   (double_to_single_valid_out),
        .m_axis_result_tdata    (double_to_single_data_out)
    );

    // Single to Double FSM
    enum logic [1 : 0]  {IDLE,
                        CONVERT_LEFT_CHANNEL,
                        CONVERT_RIGHT_CHANNEL} double_to_single_fsm_state = IDLE;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] data_right = 'b0;

    always_ff @(posedge i_clock) begin : double_to_single_fsm
        case (double_to_single_fsm_state)
            IDLE : begin
                o_data_valid <= 1'b0;
                double_to_single_valid_in <= 1'b0;
                if (i_data_valid == 1'b1) begin
                    data_right <= i_data_right;
                    double_to_single_valid_in <= 1'b1;
                    double_to_single_data_in <= i_data_left;
                    double_to_single_fsm_state <= CONVERT_LEFT_CHANNEL;
                end
            end

            CONVERT_LEFT_CHANNEL : begin
                double_to_single_valid_in <= 1'b0;
                if (double_to_single_valid_out == 1'b1) begin
                    o_data_left <= double_to_single_data_out;
                    double_to_single_valid_in <= 1'b1;
                    double_to_single_data_in <= data_right;
                    double_to_single_fsm_state <= CONVERT_RIGHT_CHANNEL;
                end
            end

            CONVERT_RIGHT_CHANNEL : begin
                double_to_single_valid_in <= 1'b0;
                if (double_to_single_valid_out == 1'b1) begin
                    o_data_right <= double_to_single_data_out;
                    o_data_valid <= 1'b1;
                    double_to_single_fsm_state <= IDLE;
                end
            end

            default : begin
                double_to_single_fsm_state <= IDLE;
            end
        endcase
    end

endmodule
