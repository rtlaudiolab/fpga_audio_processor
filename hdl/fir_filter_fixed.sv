module fir_filter_fixed (
    input   logic                   i_clock,
    // Audio Input
    input   logic                   i_data_valid,
    input   logic signed [23 : 0]   i_data_left,
    input   logic signed [23 : 0]   i_data_right,
    // Audio Output
    output  logic                   o_data_valid,
    output  logic signed [23 : 0]   o_data_left,
    output  logic signed [23 : 0]   o_data_right
);

    timeunit 1ns;
    timeprecision 1ps;

    fir_filter_fixed_fsm  fir_filter_fixed_fsm_left_inst (
        .i_clock        (i_clock),
        .i_data_valid   (i_data_valid),
        .i_data         (i_data_left),
        .o_data_valid   (o_data_valid),
        .o_data         (o_data_left)
    );

    fir_filter_fixed_fsm  fir_filter_fixed_fsm_right_inst (
        .i_clock        (i_clock),
        .i_data_valid   (i_data_valid),
        .i_data         (i_data_right),
        .o_data         (o_data_right)
    );

endmodule
