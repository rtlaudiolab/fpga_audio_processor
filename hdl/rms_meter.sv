//! The RMS Meter module calculates the root mean square (RMS) value of the incoming signal.
module rms_meter (
    input   logic i_clock,                          //! System clock
    // Audio Input
    input   logic   [31 : 0]    i_data,             //! Incoming audio sample in single-precision floating-point format
    input   logic               i_data_valid,       //! Signal indicating that the value on the i_data bus is valid. Remains enabled for one cycle of i_clock
    // RMS Value Output
    output  logic   [31 : 0]    o_rms_value,        //! Calculated RMS value
    output  logic               o_rms_value_valid   //! Signal indicating that the value on the o_rms_value bus is valid. Remains enabled for one cycle of i_clock
);

    timeunit 1ns;
    timeprecision 1ps;

    localparam shortreal BUFFER_SIZE = 32'h44800000;    // 1024

    // Buffer
    logic           buffer_wr_en = 1'b0;
    logic [9 : 0]   buffer_data_in_addr = 'b0;
    logic [31 : 0]  buffer_data_in = 'b0;
    logic [9 : 0]   buffer_data_out_addr = 'b0;
    logic [31 : 0]  buffer_data_out;
    rms_meter_buffer rms_meter_buffer_inst(
        .clka   (i_clock),
        .wea    (buffer_wr_en),
        .addra  (buffer_data_in_addr),
        .dina   (buffer_data_in),
        .clkb   (i_clock),
        .addrb  (buffer_data_out_addr),
        .doutb  (buffer_data_out)
    );

    // Floating-point Multiplier
    logic           fp_mult_valid_in;
    logic [31 : 0]  fp_mult_data_a;
    logic [31 : 0]  fp_mult_data_b;
    logic           fp_mult_valid_out;
    logic [31 : 0]  fp_mult_data_out;
    fp_multiplier fp_multiplier_inst(
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (fp_mult_valid_in),
        .s_axis_a_tdata         (fp_mult_data_a),
        .s_axis_b_tvalid        (fp_mult_valid_in),
        .s_axis_b_tdata         (fp_mult_data_b),
        .m_axis_result_tvalid   (fp_mult_valid_out),
        .m_axis_result_tdata    (fp_mult_data_out)
    );

    // Floating-point Adder
    logic           fp_adder_valid_in;
    logic [31 : 0]  fp_adder_data_a;
    logic [31 : 0]  fp_adder_data_b;
    logic           fp_adder_valid_out;
    logic [31 : 0]  fp_adder_data_out;
    fp_adder fp_adder_inst(
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (fp_adder_valid_in),
        .s_axis_a_tdata         (fp_adder_data_a),
        .s_axis_b_tvalid        (fp_adder_valid_in),
        .s_axis_b_tdata         (fp_adder_data_b),
        .m_axis_result_tvalid   (fp_adder_valid_out),
        .m_axis_result_tdata    (fp_adder_data_out)
    );

    // Floating-point Subtractor
    logic           fp_subtractor_valid_in;
    logic [31 : 0]  fp_subtractor_data_a;
    logic [31 : 0]  fp_subtractor_data_b;
    logic           fp_subtractor_valid_out;
    logic [31 : 0]  fp_subtractor_data_out;
    fp_subtractor fp_subtractor_inst(
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (fp_subtractor_valid_in),
        .s_axis_a_tdata         (fp_subtractor_data_a),
        .s_axis_b_tvalid        (fp_subtractor_valid_in),
        .s_axis_b_tdata         (fp_subtractor_data_b),
        .m_axis_result_tvalid   (fp_subtractor_valid_out),
        .m_axis_result_tdata    (fp_subtractor_data_out)
    );

    // Floating-point Divider
    logic           fp_divider_valid_in;
    logic [31 : 0]  fp_divider_data_a;
    logic [31 : 0]  fp_divider_data_b;
    logic           fp_divider_valid_out;
    logic [31 : 0]  fp_divider_data_out;
    fp_divider fp_divider_inst(
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (fp_divider_valid_in),
        .s_axis_a_tdata         (fp_divider_data_a),
        .s_axis_b_tvalid        (fp_divider_valid_in),
        .s_axis_b_tdata         (fp_divider_data_b),
        .m_axis_result_tvalid   (fp_divider_valid_out),
        .m_axis_result_tdata    (fp_divider_data_out)
    );

    // Floating-point Square Root
    logic           fp_square_root_valid_in;
    logic [31 : 0]  fp_square_root_data_in;
    logic [31 : 0]  fp_square_root_data_out;
    fp_square_root fp_square_root_inst(
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (fp_square_root_valid_in),
        .s_axis_a_tdata         (fp_square_root_data_in),
        .m_axis_result_tvalid   (fp_square_root_valid_out),
        .m_axis_result_tdata    (fp_square_root_data_out)
    );

    // Main FSM
    enum logic [2 : 0]  {IDLE,
                        SQUARE_NEWEST_SAMPLE,
                        ADD_NEWEST_SAMPLE,
                        FETCH_OLDEST_SAMPLE,
                        REMOVE_OLDEST_SAMPLE,
                        CALCULATE_AVERAGE,
                        EXTRACT_SQUARE_ROOT} main_fsm_state = IDLE;

    logic [31 : 0] accumulator = 'b0;
    logic [31 : 0] aux = 'b0;

    always_ff @(posedge i_clock) begin : main_fsm
        buffer_data_out_addr <= buffer_data_in_addr + 1;
        case (main_fsm_state)
            IDLE : begin
                o_rms_value_valid <= 1'b0;
                buffer_wr_en <= 1'b0;
                fp_mult_valid_in <= 1'b0;
                fp_adder_valid_in <= 1'b0;
                fp_subtractor_valid_in <= 1'b0;
                fp_divider_valid_in <= 1'b0;
                fp_square_root_valid_in <= 1'b0;
                if (i_data_valid == 1'b1) begin
                    fp_mult_valid_in <= 1'b1;
                    fp_mult_data_a <= i_data;
                    fp_mult_data_b <= i_data;
                    buffer_data_in_addr <= buffer_data_in_addr + 1;
                    main_fsm_state <= SQUARE_NEWEST_SAMPLE;
                end
            end

            SQUARE_NEWEST_SAMPLE : begin
                fp_mult_valid_in <= 1'b0;
                if (fp_mult_valid_out == 1'b1) begin
                    buffer_wr_en <= 1'b1;
                    buffer_data_in <= fp_mult_data_out;
                    fp_adder_valid_in <= 1'b1;
                    fp_adder_data_a <= fp_mult_data_out;
                    fp_adder_data_b <= accumulator;
                    main_fsm_state <= ADD_NEWEST_SAMPLE;
                end
            end

            ADD_NEWEST_SAMPLE : begin
                buffer_wr_en <= 1'b0;
                fp_adder_valid_in <= 1'b0;
                if (fp_adder_valid_out == 1'b1) begin
                    fp_subtractor_valid_in <= 1'b1;
                    fp_subtractor_data_a <= fp_adder_data_out;
                    fp_subtractor_data_b <= buffer_data_out;
                    main_fsm_state <= REMOVE_OLDEST_SAMPLE;
                end
            end

            REMOVE_OLDEST_SAMPLE : begin
                fp_subtractor_valid_in <= 1'b0;
                if (fp_subtractor_valid_out == 1'b1) begin
                    accumulator <= fp_subtractor_data_out;
                    fp_divider_valid_in <= 1'b1;
                    fp_divider_data_a <= fp_subtractor_data_out;
                    fp_divider_data_b <= BUFFER_SIZE;
                    main_fsm_state <= CALCULATE_AVERAGE;
                end
            end

            CALCULATE_AVERAGE : begin
                fp_divider_valid_in <= 1'b0;
                if (fp_divider_valid_out == 1'b1) begin
                    fp_square_root_valid_in <= 1'b1;
                    fp_square_root_data_in <= fp_divider_data_out;
                    main_fsm_state <= EXTRACT_SQUARE_ROOT;
                end
            end

            EXTRACT_SQUARE_ROOT : begin
                fp_square_root_valid_in <= 1'b0;
                if (fp_square_root_valid_out == 1'b1) begin
                    o_rms_value <= fp_square_root_data_out;
                    o_rms_value_valid <= 1'b1;
                    main_fsm_state <= IDLE;
                end
            end

            default : begin
                main_fsm_state <= IDLE;
            end
        endcase
    end
endmodule
