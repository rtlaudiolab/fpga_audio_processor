module fir_filter # (
    parameter integer SP_FLOATING_POINT_BIT_WIDTH = 32
    ) (
    input   logic                                       i_clock,
    // Audio Input
    input   logic                                       i_data_valid,
    input   logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_data_left,
    input   logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_data_right,
    // Audio Output
    output  logic                                       o_data_valid,
    output  logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] o_data_left,
    output  logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] o_data_right
);

    timeunit 1ns;
    timeprecision 1ps;

    fir_filter_fsm  #(
        .SP_FLOATING_POINT_BIT_WIDTH    (SP_FLOATING_POINT_BIT_WIDTH )
    ) fir_filter_fsm_left_inst (
        .i_clock        (i_clock),
        .i_data_valid   (i_data_valid),
        .i_data         (i_data_left),
        .o_data_valid   (o_data_valid),
        .o_data         (o_data_left)
    );

    fir_filter_fsm  #(
        .SP_FLOATING_POINT_BIT_WIDTH    (SP_FLOATING_POINT_BIT_WIDTH )
    ) fir_filter_fsm_right_inst (
        .i_clock        (i_clock),
        .i_data_valid   (i_data_valid),
        .i_data         (i_data_right),
        .o_data         (o_data_right)
    );

endmodule
