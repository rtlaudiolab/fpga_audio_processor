module biquad_filter # (
    parameter integer DP_FLOATING_POINT_BIT_WIDTH = 64
    ) (
    input   logic                                       i_clock,
    // Audio Input
    input   logic                                       i_data_valid,
    input   logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_data_left,
    input   logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_data_right,
    // Audio Output
    output  logic                                       o_data_valid,
    output  logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] o_data_left,
    output  logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] o_data_right
);

    timeunit 1ns;
    timeprecision 1ps;

    // Biquad equation: y[n] = a0 * x[n] + d1
    //                  d1 = a1 * x[n-1] + b1 * y[n-1] + d2
    //                  d2 = a2 * x[n-2] + b2 * y[n-2]

    // Filter coefficients (lowpass, 44.1 kHz, 500 Hz Fc, 0.7071 Q, 6 dB Gain)
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] a0 = 64'h3F53C838DB03A294;    // a0 = 0.0012074046354035072
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] a1 = 64'h3F63C838DB03A294;    // a1 = 0.0024148092708070144
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] a2 = 64'h3F53C838DB03A294;    // a2 = 0.0012074046354035072
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] b1 = 64'h3FFE63AA866C6F75;    // b1 = 1.8993325472756315
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] b2 = 64'hBFECEEE57E8EE62E;    // b2 = -0.9041621658172454

    // Biquad Equation
    logic biquad_equation_start;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   xn_left   = 'b0;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   xn_1_left = 'b0;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   xn_2_left = 'b0;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   yn_left   = 'b0;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   yn_1_left = 'b0;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   yn_2_left = 'b0;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   xn_right = 'b0;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   xn_1_right = 'b0;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   xn_2_right = 'b0;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   yn_right = 'b0;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   yn_1_right = 'b0;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   yn_2_right = 'b0;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   biquad_equation_xn;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   biquad_equation_xn_1;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   biquad_equation_xn_2;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   biquad_equation_yn_1;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   biquad_equation_yn_2;
    logic                                       biquad_equation_done;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   biquad_equation_yn;
    biquad_equation # (
      .DP_FLOATING_POINT_BIT_WIDTH (DP_FLOATING_POINT_BIT_WIDTH)
    ) biquad_equation_inst (
      .i_clock  (i_clock),
      .i_start  (biquad_equation_start),
      .i_a0     (a0),
      .i_a1     (a1),
      .i_a2     (a2),
      .i_b1     (b1),
      .i_b2     (b2),
      .i_xn     (biquad_equation_xn),
      .i_xn_1   (biquad_equation_xn_1),
      .i_xn_2   (biquad_equation_xn_2),
      .i_yn_1   (biquad_equation_yn_1),
      .i_yn_2   (biquad_equation_yn_2),
      .o_done   (biquad_equation_done),
      .o_yn     (biquad_equation_yn)
    );
/*
    biquad_equation_hls_0 biquad_equation_hls_0_inst (
        .ap_clk     (i_clock),
        .ap_rst     (1'b0),
        .ap_start   (biquad_equation_start),
        .ap_done    (biquad_equation_done),
        .ap_return  (biquad_equation_yn),
        .i_a0       (a0),
        .i_a1       (a1),
        .i_a2       (a2),
        .i_b1       (b1),
        .i_b2       (b2),
        .i_xn       (biquad_equation_xn),
        .i_xn_1     (biquad_equation_xn_1),
        .i_xn_2     (biquad_equation_xn_2),
        .i_yn_1     (biquad_equation_yn_1),
        .i_yn_2     (biquad_equation_yn_2)
    );
*/

    // Biquad Control FSM
    enum logic [2 : 0]  {IDLE,
                        PROCESS_LEFT_CHANNEL,
                        UPDATE_LEFT_SAMPLES,
                        PROCESS_RIGHT_CHANNEL,
                        UPDATE_RIGHT_SAMPLES} biquad_control_fsm_state = IDLE;

    always_ff @(posedge i_clock) begin : biquad_control_fsm
        case (biquad_control_fsm_state)
            IDLE : begin
                biquad_equation_start <= 1'b0;
                o_data_valid <= 1'b0;
                if (i_data_valid == 1'b1) begin
                    xn_2_left <= xn_1_left;
                    xn_1_left <= xn_left;
                    xn_left <= i_data_left;
                    xn_2_right <= xn_1_right;
                    xn_1_right <= xn_right;
                    xn_right <= i_data_right;
                    biquad_control_fsm_state <= PROCESS_LEFT_CHANNEL;
                end
            end

            PROCESS_LEFT_CHANNEL : begin
                biquad_equation_xn <= xn_left;
                biquad_equation_xn_1 <= xn_1_left;
                biquad_equation_xn_2 <= xn_2_left;
                biquad_equation_yn_1 <= yn_left;
                biquad_equation_yn_2 <= yn_1_left;
                biquad_equation_start <= 1'b1;
                biquad_control_fsm_state <= UPDATE_LEFT_SAMPLES;
            end

            UPDATE_LEFT_SAMPLES : begin
                biquad_equation_start <= 1'b0;
                if (biquad_equation_done == 1'b1) begin
                    yn_2_left <= yn_1_left;
                    yn_1_left <= yn_left;
                    yn_left <= biquad_equation_yn;
                    biquad_control_fsm_state <= PROCESS_RIGHT_CHANNEL;
                end
            end

            PROCESS_RIGHT_CHANNEL : begin
                biquad_equation_xn <= xn_right;
                biquad_equation_xn_1 <= xn_1_right;
                biquad_equation_xn_2 <= xn_2_right;
                biquad_equation_yn_1 <= yn_right;
                biquad_equation_yn_2 <= yn_1_right;
                biquad_equation_start <= 1'b1;
                biquad_control_fsm_state <= UPDATE_RIGHT_SAMPLES;
            end

            UPDATE_RIGHT_SAMPLES : begin
                biquad_equation_start <= 1'b0;
                if (biquad_equation_done == 1'b1) begin
                    yn_2_right <= yn_1_right;
                    yn_1_right <= yn_right;
                    yn_right <= biquad_equation_yn;
                    o_data_valid <= 1'b1;
                    biquad_control_fsm_state <= IDLE;
                end
            end

            default : begin
                biquad_control_fsm_state <= IDLE;
            end
        endcase
    end

    assign o_data_left = yn_left;
    assign o_data_right = yn_right;

endmodule
