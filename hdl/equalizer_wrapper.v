module equalizer_wrapper # (
    parameter integer SP_FLOATING_POINT_BIT_WIDTH = 32,
    parameter integer DP_FLOATING_POINT_BIT_WIDTH = 64
    ) (
    input   wire               i_clock,
    input   wire               i_iir_enable,
    input   wire               i_fir_enable,
    // Audio Input
    input   wire               i_data_valid,
    input   wire   [31 : 0]    i_data_left,
    input   wire   [31 : 0]    i_data_right,
    // Audio Output
    output  wire               o_data_valid,
    output  wire   [31 : 0]    o_data_left,
    output  wire   [31 : 0]    o_data_right
);

    equalizer # (
        .SP_FLOATING_POINT_BIT_WIDTH    (SP_FLOATING_POINT_BIT_WIDTH),
        .DP_FLOATING_POINT_BIT_WIDTH    (DP_FLOATING_POINT_BIT_WIDTH)
    ) equalizer_inst (
        .i_clock        (i_clock),
        .i_iir_enable   (i_iir_enable),
        .i_fir_enable   (i_fir_enable),
        .i_data_valid   (i_data_valid),
        .i_data_left    (i_data_left),
        .i_data_right   (i_data_right),
        .o_data_valid   (o_data_valid),
        .o_data_left    (o_data_left),
        .o_data_right   (o_data_right)
    );

endmodule
