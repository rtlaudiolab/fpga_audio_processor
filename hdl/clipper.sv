module clipper (
    input   logic               i_clock,
    // Audio Input
    input   logic               i_data_valid,
    input   logic   [31 : 0]    i_data_left,
    input   logic   [31 : 0]    i_data_right,
    // Audio Output
    output  logic               o_data_valid,
    output  logic   [31 : 0]    o_data_left,
    output  logic   [31 : 0]    o_data_right
);

    timeunit 1ns;
    timeprecision 1ps;

    logic           fp_greater_comp_data_in_valid;
    logic [31 : 0]  fp_greater_comp_data_a_in;
    logic           fp_greater_comp_data_out_valid;
    logic [7 : 0]   fp_greater_comp_data_out;
    fp_greater_equal_comp fp_greater_equal_comp_inst (
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (fp_greater_comp_data_in_valid),
        .s_axis_a_tdata         (fp_greater_comp_data_a_in),
        .s_axis_b_tvalid        (fp_greater_comp_data_in_valid),
        .s_axis_b_tdata         (32'h4AF1ADF8),     // 7919356, ~-0.5 dBFS
        .m_axis_result_tvalid   (fp_greater_comp_data_out_valid),
        .m_axis_result_tdata    (fp_greater_comp_data_out)
    );

    logic           fp_less_comp_data_in_valid;
    logic [31 : 0]  fp_less_comp_data_a_in;
    logic           fp_less_comp_data_out_valid;
    logic [7 : 0]   fp_less_comp_data_out;
    fp_less_equal_comp fp_less_equal_comp_inst (
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (fp_less_comp_data_in_valid),
        .s_axis_a_tdata         (fp_less_comp_data_a_in),
        .s_axis_b_tvalid        (fp_less_comp_data_in_valid),
        .s_axis_b_tdata         (32'hCAF1ADF8),     // -7919356, ~-0.5 dBFS
        .m_axis_result_tvalid   (fp_less_comp_data_out_valid),
        .m_axis_result_tdata    (fp_less_comp_data_out)
    );

    // Clipper FSM
    enum logic [2 : 0]  {IDLE,
                        CHECK_LEFT_CHANNEL_POSITIVE,
                        CHECK_LEFT_CHANNEL_NEGATIVE,
                        CHECK_RIGHT_CHANNEL_POSITIVE,
                        CHECK_RIGHT_CHANNEL_NEGATIVE} clipper_fsm_state = IDLE;
    logic [31 : 0] data_left = 'b0;
    logic [31 : 0] data_right = 'b0;

    always_ff @(posedge i_clock) begin : clipper_fsm
        case (clipper_fsm_state)
            IDLE : begin
                o_data_valid <= 1'b0;
                fp_greater_comp_data_in_valid <= 1'b0;
                fp_less_comp_data_in_valid <= 1'b0;
                if (i_data_valid == 1'b1) begin
                    data_left <= i_data_left;
                    data_right <= i_data_right;
                    fp_greater_comp_data_in_valid <= 1'b1;
                    fp_greater_comp_data_a_in <= i_data_left;
                    clipper_fsm_state <= CHECK_LEFT_CHANNEL_POSITIVE;
                end
            end

            CHECK_LEFT_CHANNEL_POSITIVE : begin
                fp_greater_comp_data_in_valid <= 1'b0;
                if (fp_greater_comp_data_out_valid == 1'b1) begin
                    if (fp_greater_comp_data_out[0] == 1'b1) begin
                        o_data_left <= 32'h4AF1ADFA;    // 7919357
                        fp_greater_comp_data_in_valid <= 1'b1;
                        fp_greater_comp_data_a_in <= data_right;
                        clipper_fsm_state <= CHECK_RIGHT_CHANNEL_POSITIVE;
                    end else begin
                        fp_less_comp_data_in_valid <= 1'b1;
                        fp_less_comp_data_a_in <= data_left;
                        clipper_fsm_state <= CHECK_LEFT_CHANNEL_NEGATIVE;
                    end
                end
            end

            CHECK_LEFT_CHANNEL_NEGATIVE : begin
                fp_less_comp_data_in_valid <= 1'b0;
                if (fp_less_comp_data_out_valid == 1'b1) begin
                    if (fp_less_comp_data_out[0] == 1'b1) begin
                        o_data_left <= 32'hCAF1ADFA;    // -7919357
                    end else begin
                        o_data_left <= data_left;
                    end
                    fp_greater_comp_data_in_valid <= 1'b1;
                    fp_greater_comp_data_a_in <= data_right;
                    clipper_fsm_state <= CHECK_RIGHT_CHANNEL_POSITIVE;
                end
            end

            CHECK_RIGHT_CHANNEL_POSITIVE : begin
                fp_greater_comp_data_in_valid <= 1'b0;
                if (fp_greater_comp_data_out_valid == 1'b1) begin
                    if (fp_greater_comp_data_out[0] == 1'b1) begin
                        o_data_right <= 32'h4AF1ADFA;    // 7919357
                        o_data_valid <= 1'b1;
                        clipper_fsm_state <= IDLE;
                    end else begin
                        fp_less_comp_data_in_valid <= 1'b1;
                        fp_less_comp_data_a_in <= data_right;
                        clipper_fsm_state <= CHECK_RIGHT_CHANNEL_NEGATIVE;
                    end
                end
            end

            CHECK_RIGHT_CHANNEL_NEGATIVE : begin
                fp_less_comp_data_in_valid <= 1'b0;
                if (fp_less_comp_data_out_valid == 1'b1) begin
                    if (fp_less_comp_data_out[0] == 1'b1) begin
                        o_data_right <= 32'hCAF1ADFA;    // -7919357
                    end else begin
                        o_data_right <= data_right;
                    end
                    o_data_valid <= 1'b1;
                    clipper_fsm_state <= IDLE;
                end
            end

            default : begin
                clipper_fsm_state <= IDLE;
            end
        endcase
    end

endmodule
