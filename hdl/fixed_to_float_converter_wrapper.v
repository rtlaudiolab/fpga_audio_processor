module fixed_to_float_converter_wrapper # (
    parameter integer FIXED_POINT_BIT_WIDTH = 24,
    parameter integer FLOATING_POINT_BIT_WIDTH = 32
    ) (
    input   wire                                       i_clock,
    // Audio Input
    input   wire                                       i_data_valid,
    input   wire   [FIXED_POINT_BIT_WIDTH-1 : 0]       i_data_left,
    input   wire   [FIXED_POINT_BIT_WIDTH-1 : 0]       i_data_right,
    // Audio Output
    output  wire                                       o_data_valid,
    output  wire   [FLOATING_POINT_BIT_WIDTH-1 : 0]    o_data_left,
    output  wire   [FLOATING_POINT_BIT_WIDTH-1 : 0]    o_data_right
);

    fixed_to_float_converter # (
        .FIXED_POINT_BIT_WIDTH  (FIXED_POINT_BIT_WIDTH),
        .FLOATING_POINT_BIT_WIDTH (FLOATING_POINT_BIT_WIDTH)
    ) fixed_to_float_converter_inst (
    .i_clock        (i_clock),
    .i_data_valid   (i_data_valid),
    .i_data_left    (i_data_left),
    .i_data_right   (i_data_right),
    .o_data_valid   (o_data_valid),
    .o_data_left    (o_data_left),
    .o_data_right   (o_data_right)
    );

endmodule
