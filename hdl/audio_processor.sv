module audio_processor (
    input   logic           i_clock,
    // Audio Interface
    input   logic           i_codec_bit_clock,
    input   logic           i_codec_lr_clock,
    input   logic           i_codec_adc_data,
    output  logic           o_codec_dac_data,
    // Input switches
    input   logic           i_sw0,
    input   logic           i_sw1,
    input   logic           i_sw2,
    input   logic           i_sw3,
    input   logic           i_sw4,
    input   logic           i_sw5,
    input   logic           i_sw6,
    input   logic           i_sw7,
    // Buttons
    input   logic           i_btnu,
    input   logic           i_btnd,
    input   logic           i_btnl,
    input   logic           i_btnr,
    // LEDs
    output  logic [7 : 0]   o_led
);

    timeunit 1ns;
    timeprecision 1ps;

    // Audio Deserializer
    logic [23 : 0]  deser_data_left;
    logic [23 : 0]  deser_data_right;
    logic           deser_data_valid;
    audio_deserializer audio_deserializer_inst (
        .i_clock            (i_clock),
        // I2S Interface
        .i_codec_bit_clock  (i_codec_bit_clock),
        .i_codec_lr_clock   (i_codec_lr_clock),
        .i_codec_adc_data   (i_codec_adc_data),
        // Parallel Data Output
        .o_data_left        (deser_data_left),
        .o_data_right       (deser_data_right),
        .o_data_valid       (deser_data_valid)
    );

    // Fixed-point FIR Filter
    logic [23 : 0]  fir_filter_fixed_data_left;
    logic [23 : 0]  fir_filter_fixed_data_right;
    logic           fir_filter_fixed_data_valid;
    fir_filter_fixed fir_filter_fixed_inst (
        .i_clock        (i_clock),
        // Audio Input
        .i_data_valid   (deser_data_valid),
        .i_data_left    (deser_data_left),
        .i_data_right   (deser_data_right),
        // Audio Output
        .o_data_valid   (fir_filter_fixed_data_valid),
        .o_data_left    (fir_filter_fixed_data_left),
        .o_data_right   (fir_filter_fixed_data_right)
    );

    // LED Meter
    led_meter led_meter_inst (
        .i_clock        (i_clock),
        // Audio Input
        .i_data_left    (fir_filter_fixed_data_left),
        .i_data_right   (fir_filter_fixed_data_right),
        .i_data_valid   (fir_filter_fixed_data_valid),
        // LED Meter Output
        .o_led          (o_led)
    );

    // Audio Serializer
    audio_serializer audio_serializer_inst (
        .i_clock            (i_clock),
        // I2S Interface
        .i_codec_bit_clock  (i_codec_bit_clock),
        .i_codec_lr_clock   (i_codec_lr_clock),
        .o_codec_dac_data   (o_codec_dac_data),
        // Parallel Data Input
        .i_data_left        (fir_filter_fixed_data_left),
        .i_data_right       (fir_filter_fixed_data_right),
        .i_data_valid       (fir_filter_fixed_data_valid)
    );

endmodule
