module limiter_fsm # (
    parameter integer SP_FLOATING_POINT_BIT_WIDTH = 32
    ) (
    input   logic                                       i_clock,
    input   logic                                       i_enable,
    // Audio Input
    input   logic                                       i_data_valid,
    input   logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_data,
    // Controls
    input   logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_linear_threshold,
    // Audio Output
    output  logic                                       o_data_valid,
    output  logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] o_data
);

    timeunit 1ns;
    timeprecision 1ps;

    parameter logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] ATTACK_TIME = 32'h3F666666;    // 0.9
    parameter logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] RELEASE_TIME = 32'h3C23D70A;   // 0.01
    parameter logic [6 : 0]                             LOOKAHEAD_SAMPLE_COUNT = 6;

    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] data;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] xpeak;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] gain;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] coefficient;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] filter;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] absolute_value;

    logic                                       fp_abs_value_valid_in;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_abs_value_data_in;
    logic                                       fp_abs_value_valid_out;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_abs_value_data_out;
    fp_absolute_value fp_absolute_value_inst (
      .s_axis_a_tvalid      (fp_abs_value_valid_in),
      .s_axis_a_tdata       (fp_abs_value_data_in),
      .m_axis_result_tvalid (fp_abs_value_valid_out),
      .m_axis_result_tdata  (fp_abs_value_data_out)
    );

    logic                                       fp_greater_comp_valid_in;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_greater_comp_data_a_in;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_greater_comp_data_b_in;
    logic                                       fp_greater_comp_valid_out;
    logic [7 : 0]                               fp_greater_comp_data_out;
    fp_greater_comp fp_greater_comp_inst (
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (fp_greater_comp_valid_in),
        .s_axis_a_tdata         (fp_greater_comp_data_a_in),
        .s_axis_b_tvalid        (fp_greater_comp_valid_in),
        .s_axis_b_tdata         (fp_greater_comp_data_b_in),
        .m_axis_result_tvalid   (fp_greater_comp_valid_out),
        .m_axis_result_tdata    (fp_greater_comp_data_out)
    );

    logic                                       fp_subtractor_valid_in;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_subtractor_data_a_in;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_subtractor_data_b_in;
    logic                                       fp_subtractor_valid_out;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_subtractor_data_out;
    fp_subtractor fp_subtractor_inst (
      .aclk                 (i_clock),
      .s_axis_a_tvalid      (fp_subtractor_valid_in),
      .s_axis_a_tdata       (fp_subtractor_data_a_in),
      .s_axis_b_tvalid      (fp_subtractor_valid_in),
      .s_axis_b_tdata       (fp_subtractor_data_b_in),
      .m_axis_result_tvalid (fp_subtractor_valid_out),
      .m_axis_result_tdata  (fp_subtractor_data_out)
    );

    logic                                       fp_mult_valid_in;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_mult_data_in_a;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_mult_data_in_b;
    logic                                       fp_mult_valid_out;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_mult_data_out;
    fp_multiplier fp_multiplier_inst (
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (fp_mult_valid_in),
        .s_axis_a_tdata         (fp_mult_data_in_a),
        .s_axis_b_tvalid        (fp_mult_valid_in),
        .s_axis_b_tdata         (fp_mult_data_in_b),
        .m_axis_result_tvalid   (fp_mult_valid_out),
        .m_axis_result_tdata    (fp_mult_data_out)
    );

    logic                                       fp_adder_valid_in;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_adder_data_in_a;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_adder_data_in_b;
    logic                                       fp_adder_valid_out;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_adder_data_out;
    fp_adder fp_adder_inst (
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (fp_adder_valid_in),
        .s_axis_a_tdata         (fp_adder_data_in_a),
        .s_axis_b_tvalid        (fp_adder_valid_in),
        .s_axis_b_tdata         (fp_adder_data_in_b),
        .m_axis_result_tvalid   (fp_adder_valid_out),
        .m_axis_result_tdata    (fp_adder_data_out)
    );

    logic                                       fp_divider_valid_in;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_divider_data_in_a;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_divider_data_in_b;
    logic                                       fp_divider_valid_out;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   fp_divider_data_out;
    fp_divider fp_divider_inst (
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (fp_divider_valid_in),
        .s_axis_a_tdata         (fp_divider_data_in_a),
        .s_axis_b_tvalid        (fp_divider_valid_in),
        .s_axis_b_tdata         (fp_divider_data_in_b),
        .m_axis_result_tvalid   (fp_divider_valid_out),
        .m_axis_result_tdata    (fp_divider_data_out)
    );

    logic                                       lookahead_fifo_rd_en;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   lookahead_fifo_data_out;
    logic                                       lookahead_fifo_full;
    logic                                       lookahead_fifo_empty;
    logic [6 : 0]                               lookahead_fifo_data_count;
    lookahead_fifo lookahead_fifo_inst (
        .clk        (i_clock),
        .din        (i_data),
        .wr_en      (i_data_valid),
        .rd_en      (lookahead_fifo_rd_en),
        .dout       (lookahead_fifo_data_out),
        .full       (lookahead_fifo_full),
        .empty      (lookahead_fifo_empty),
        .data_count (lookahead_fifo_data_count)
    );

    typedef enum logic [5 : 0]  {IDLE,
                                WAIT_SAMPLE,
                                GET_ABS_VALUE,
                                COMPARE_XPEAK,
                                CALC_XPEAK_1,
                                CALC_XPEAK_2,
                                CALC_XPEAK_3,
                                CALC_XPEAK_4,
                                THRESHOLD_XPEAK_RATIO,
                                CALC_FILTER,
                                COMPARE_FILTER,
                                COMPARE_FILTER_GAIN,
                                CALC_GAIN_1,
                                CALC_GAIN_2,
                                CALC_GAIN_3,
                                CALC_GAIN_4,
                                CALC_OUTPUT,
                                DRIVE_OUTPUT} limiter_fsm_state_t;
    limiter_fsm_state_t limiter_fsm_state = IDLE;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] aux;

    always_ff @(posedge i_clock) begin
        case (limiter_fsm_state)
            IDLE : begin
                xpeak <= 32'h00000000;
                gain <= 32'h3F800000;
                fp_abs_value_valid_in <= 1'b0;
                fp_greater_comp_valid_in <= 1'b0;
                fp_subtractor_valid_in <= 1'b0;
                fp_mult_valid_in <= 1'b0;
                fp_adder_valid_in <= 1'b0;
                fp_divider_valid_in <= 1'b0;
                lookahead_fifo_rd_en <= 1'b0;
                o_data_valid <= 1'b0;
                data <= 32'h00000000;
                if (i_enable == 1'b1) begin
                    limiter_fsm_state <= WAIT_SAMPLE;
                end
            end

            WAIT_SAMPLE : begin
                o_data_valid <= 1'b0;
                if (i_data_valid == 1'b1) begin
                    data <= i_data;
                    fp_abs_value_data_in <= i_data;
                    fp_abs_value_valid_in <= 1'b1;
                    limiter_fsm_state <= GET_ABS_VALUE;
                end
            end

            GET_ABS_VALUE : begin
                fp_abs_value_valid_in <= 1'b0;
                if (fp_abs_value_valid_out == 1'b1) begin
                    absolute_value <= fp_abs_value_data_out;
                    fp_greater_comp_data_a_in <= fp_abs_value_data_out;
                    fp_greater_comp_data_b_in <= xpeak;
                    fp_greater_comp_valid_in <= 1'b1;
                    limiter_fsm_state <= COMPARE_XPEAK;
                end
            end

            COMPARE_XPEAK : begin
                fp_greater_comp_valid_in <= 1'b0;
                if (fp_greater_comp_valid_out == 1'b1) begin
                    if (fp_greater_comp_data_out[0] == 1'b1) begin
                        coefficient <= ATTACK_TIME;
                    end else begin
                        coefficient <= RELEASE_TIME;
                    end
                    limiter_fsm_state <= CALC_XPEAK_1;
                end
            end

            CALC_XPEAK_1 : begin
                fp_subtractor_valid_in <= 1'b1;
                fp_subtractor_data_a_in <= 32'h3F800000;
                fp_subtractor_data_b_in <= coefficient;
                limiter_fsm_state <= CALC_XPEAK_2;
            end

            CALC_XPEAK_2 : begin
                fp_subtractor_valid_in <= 1'b0;
                if (fp_subtractor_valid_out == 1'b1) begin
                    fp_mult_valid_in <= 1'b1;
                    fp_mult_data_in_a <= fp_subtractor_data_out;
                    fp_mult_data_in_b <= xpeak;
                    limiter_fsm_state <= CALC_XPEAK_3;
                end
            end

            CALC_XPEAK_3 : begin
                fp_mult_valid_in <= 1'b0;
                if (fp_mult_valid_out == 1'b1) begin
                    aux <= fp_mult_data_out;
                    fp_mult_valid_in <= 1'b1;
                    fp_mult_data_in_a <= coefficient;
                    fp_mult_data_in_b <= absolute_value;
                    limiter_fsm_state <= CALC_XPEAK_4;
                end
            end

            CALC_XPEAK_4 : begin
                fp_mult_valid_in <= 1'b0;
                if (fp_mult_valid_out == 1'b1) begin
                    fp_adder_valid_in <= 1'b1;
                    fp_adder_data_in_a <= aux;
                    fp_adder_data_in_b <= fp_mult_data_out;
                    limiter_fsm_state <= THRESHOLD_XPEAK_RATIO;
                end
            end

            THRESHOLD_XPEAK_RATIO : begin
                fp_adder_valid_in <= 1'b0;
                if (fp_adder_valid_out) begin
                    xpeak <= fp_adder_data_out;
                    fp_divider_valid_in <= 1'b1;
                    fp_divider_data_in_a <= i_linear_threshold;
                    fp_divider_data_in_b <= fp_adder_data_out;
                    limiter_fsm_state <= CALC_FILTER;
                end
            end

            CALC_FILTER : begin
                fp_divider_valid_in <= 1'b0;
                if (fp_divider_valid_out == 1'b1) begin
                    filter <= fp_divider_data_out;
                    fp_greater_comp_valid_in <= 1'b1;
                    fp_greater_comp_data_a_in <= fp_divider_data_out;
                    fp_greater_comp_data_b_in <= 32'h3F800000;
                    limiter_fsm_state <= COMPARE_FILTER;
                end
            end

            COMPARE_FILTER : begin
                fp_greater_comp_valid_in <= 1'b0;
                if (fp_greater_comp_valid_out == 1'b1) begin
                    if (fp_greater_comp_data_out[0] == 1'b1) begin
                        filter <= 32'h3F800000;
                        fp_greater_comp_data_b_in <= 32'h3F800000;
                    end else begin
                        fp_greater_comp_data_b_in <= filter;
                    end
                    fp_greater_comp_valid_in <= 1'b1;
                    fp_greater_comp_data_a_in <= gain;
                    limiter_fsm_state <= COMPARE_FILTER_GAIN;
                end
            end

            COMPARE_FILTER_GAIN : begin
                fp_greater_comp_valid_in <= 1'b0;
                if (fp_greater_comp_valid_out == 1'b1) begin
                    if (fp_greater_comp_data_out[0] == 1'b1) begin
                        coefficient <= ATTACK_TIME;
                    end else begin
                        coefficient <= RELEASE_TIME;
                    end
                    limiter_fsm_state <= CALC_GAIN_1;
                end
            end

            CALC_GAIN_1 : begin
                fp_subtractor_valid_in <= 1'b1;
                fp_subtractor_data_a_in <= 32'h3F800000;
                fp_subtractor_data_b_in <= coefficient;
                limiter_fsm_state <= CALC_GAIN_2;
            end

            CALC_GAIN_2 : begin
                fp_subtractor_valid_in <= 1'b0;
                if (fp_subtractor_valid_out == 1'b1) begin
                    fp_mult_valid_in <= 1'b1;
                    fp_mult_data_in_a <= fp_subtractor_data_out;
                    fp_mult_data_in_b <= gain;
                    limiter_fsm_state <= CALC_GAIN_3;
                end
            end

            CALC_GAIN_3 : begin
                fp_mult_valid_in <= 1'b0;
                if (fp_mult_valid_out == 1'b1) begin
                    aux <= fp_mult_data_out;
                    fp_mult_valid_in <= 1'b1;
                    fp_mult_data_in_a <= coefficient;
                    fp_mult_data_in_b <= filter;
                    limiter_fsm_state <= CALC_GAIN_4;
                end
            end

            CALC_GAIN_4 : begin
                fp_mult_valid_in <= 1'b0;
                if (fp_mult_valid_out == 1'b1) begin
                    fp_adder_valid_in <= 1'b1;
                    fp_adder_data_in_a <= aux;
                    fp_adder_data_in_b <= fp_mult_data_out;
                    limiter_fsm_state <= CALC_OUTPUT;
                end
            end

            CALC_OUTPUT : begin
                fp_adder_valid_in <= 1'b0;
                if (fp_adder_valid_out == 1'b1) begin
                    gain <= fp_adder_data_out;
                    if (lookahead_fifo_data_count > LOOKAHEAD_SAMPLE_COUNT) begin
                        lookahead_fifo_rd_en <= 1'b1;
                        fp_mult_valid_in <= 1'b1;
                        fp_mult_data_in_a <= fp_adder_data_out;
                        fp_mult_data_in_b <= lookahead_fifo_data_out;
                        limiter_fsm_state <= DRIVE_OUTPUT;
                    end else begin
                        limiter_fsm_state <= WAIT_SAMPLE;
                    end
                end
            end

            DRIVE_OUTPUT : begin
                lookahead_fifo_rd_en <= 1'b0;
                fp_mult_valid_in <= 1'b0;
                if (fp_mult_valid_out == 1'b1) begin
                    o_data_valid <= 1'b1;
                    o_data <= fp_mult_data_out;
                    limiter_fsm_state <= WAIT_SAMPLE;
                end
            end

            default : begin
                limiter_fsm_state <= IDLE;
            end
        endcase
    end

endmodule
