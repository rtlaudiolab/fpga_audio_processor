module limiter # (
    parameter integer SP_FLOATING_POINT_BIT_WIDTH = 32
    ) (
    input   logic                                           i_clock,
    input   logic                                           i_enable,
    // Audio Input
    input   logic                                           i_data_valid,
    input   logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]     i_data_left,
    input   logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]     i_data_right,
    // Controls
    input   logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]     i_linear_threshold,
    // Audio Output
    output  logic                                           o_data_valid,
    output  logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]     o_data_left,
    output  logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]     o_data_right
);

    timeunit 1ns;
    timeprecision 1ps;

    logic                                       data_valid;
    logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] data_left;
    limiter_fsm # (
        .SP_FLOATING_POINT_BIT_WIDTH    (SP_FLOATING_POINT_BIT_WIDTH)
    ) limiter_fsm_left (
        .i_clock            (i_clock),
        .i_enable           (i_enable),
        .i_data_valid       (i_data_valid),
        .i_data             (i_data_left),
        .i_linear_threshold (i_linear_threshold),
        .o_data_valid       (data_valid),
        .o_data             (data_left)
    );

    logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] data_right;
    limiter_fsm # (
        .SP_FLOATING_POINT_BIT_WIDTH    (SP_FLOATING_POINT_BIT_WIDTH)
    ) limiter_fsm_right (
        .i_clock            (i_clock),
        .i_enable           (i_enable),
        .i_data_valid       (i_data_valid),
        .i_data             (i_data_right),
        .i_linear_threshold (i_linear_threshold),
        .o_data             (data_right)
    );

    always_comb begin
        if (i_enable == 1'b1) begin
            o_data_valid = data_valid;
            o_data_left = data_left;
            o_data_right = data_right;
        end else begin
            o_data_valid = i_data_valid;
            o_data_left = i_data_left;
            o_data_right = i_data_right;
        end
    end

endmodule
