module biquad_equation # (
    parameter integer DP_FLOATING_POINT_BIT_WIDTH = 64
    ) (
    input   logic                                       i_clock,
    input   logic                                       i_start,
    // Coefficients
    input   logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_a0,
    input   logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_a1,
    input   logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_a2,
    input   logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_b1,
    input   logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_b2,
    // Samples
    input   logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_xn,
    input   logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_xn_1,
    input   logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_xn_2,
    input   logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_yn_1,
    input   logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_yn_2,
    // Audio Output
    output  logic                                       o_done,
    output  logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] o_yn
);

    timeunit 1ns;
    timeprecision 1ps;

    // Biquad equation: y[n] = a0 * x[n] + d1
    //                  d1 = a1 * x[n-1] + b1 * y[n-1] + d2
    //                  d2 = a2 * x[n-2] + b2 * y[n-2]
 
    logic                                       adder_data_in_valid;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   adder_data_in_a;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   adder_data_in_b;
    logic                                       adder_result_valid;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   adder_result;
    dp_fp_adder dp_fp_adder_inst (
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (adder_data_in_valid),
        .s_axis_a_tdata         (adder_data_in_a),
        .s_axis_b_tvalid        (adder_data_in_valid),
        .s_axis_b_tdata         (adder_data_in_b),
        .m_axis_result_tvalid   (adder_result_valid),
        .m_axis_result_tdata    (adder_result)
    );

    logic                                       mult_data_in_valid;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   mult_data_in_a;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   mult_data_in_b;
    logic                                       mult_result_valid;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   mult_result;
    dp_fp_multiplier dp_fp_multiplier_inst (
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (mult_data_in_valid),
        .s_axis_a_tdata         (mult_data_in_a),
        .s_axis_b_tvalid        (mult_data_in_valid),
        .s_axis_b_tdata         (mult_data_in_b),
        .m_axis_result_tvalid   (mult_result_valid),
        .m_axis_result_tdata    (mult_result)
        );

        // Biquad Equation FSM
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   aux;
    enum logic [3 : 0]  {IDLE,
                        MULT_A2_XN2,
                        MULT_B2_YN2,
                        ADD_D2,
                        MULT_A1_XN1,
                        ADD_D1_AUX,
                        MULT_B1_YN1,
                        ADD_D1,
                        MULT_A0_XN,
                        ADD_YN} biquad_equation_fsm_state = IDLE;
    always_ff @(posedge i_clock) begin : biquad_equation_fsm
        case (biquad_equation_fsm_state)
            IDLE : begin
                o_done <= 1'b0;
                adder_data_in_valid <= 1'b0;
                mult_data_in_valid <= 1'b0;
                if (i_start == 1'b1) begin
                    mult_data_in_a <= i_a2;
                    mult_data_in_b <= i_xn_2;
                    mult_data_in_valid <= 1'b1;
                    biquad_equation_fsm_state <= MULT_A2_XN2;
                end
            end

            MULT_A2_XN2 : begin
                mult_data_in_valid <= 1'b0;
                if (mult_result_valid == 1'b1) begin
                    aux <= mult_result;
                    mult_data_in_a <= i_b2;
                    mult_data_in_b <= i_yn_2;
                    mult_data_in_valid <= 1'b1;
                    biquad_equation_fsm_state <= MULT_B2_YN2;
                end
            end

            MULT_B2_YN2 : begin
                mult_data_in_valid <= 1'b0;
                if (mult_result_valid == 1'b1) begin
                    adder_data_in_a <= mult_result;
                    adder_data_in_b <= aux;
                    adder_data_in_valid <= 1'b1;
                    biquad_equation_fsm_state <= ADD_D2;
                end
            end

            ADD_D2 : begin
                adder_data_in_valid <= 1'b0;
                if (adder_result_valid == 1'b1) begin
                    aux <= adder_result;                // aux holds d2
                    mult_data_in_a <= i_a1;
                    mult_data_in_b <= i_xn_1;
                    mult_data_in_valid <= 1'b1;
                    biquad_equation_fsm_state <= MULT_A1_XN1;
                end
            end

            MULT_A1_XN1 : begin
                mult_data_in_valid <= 1'b0;
                if (mult_result_valid == 1'b1) begin
                    adder_data_in_a <= mult_result;
                    adder_data_in_b <= aux;
                    adder_data_in_valid <= 1'b1;
                    biquad_equation_fsm_state <= ADD_D1_AUX;
                end
            end

            ADD_D1_AUX : begin
                adder_data_in_valid <= 1'b0;
                if (adder_result_valid == 1'b1) begin
                    aux <= adder_result;
                    mult_data_in_a <= i_b1;
                    mult_data_in_b <= i_yn_1;
                    mult_data_in_valid <= 1'b1;
                    biquad_equation_fsm_state <= MULT_B1_YN1;
                end
            end

            MULT_B1_YN1 : begin
                mult_data_in_valid <= 1'b0;
                if (mult_result_valid == 1'b1) begin
                    adder_data_in_a <= mult_result;
                    adder_data_in_b <= aux;
                    adder_data_in_valid <= 1'b1;
                    biquad_equation_fsm_state <= ADD_D1;
                end
            end

            ADD_D1 : begin
                adder_data_in_valid <= 1'b0;
                if (adder_result_valid == 1'b1) begin
                    aux <= adder_result;                // aux holds d1
                    mult_data_in_a <= i_a0;
                    mult_data_in_b <= i_xn;
                    mult_data_in_valid <= 1'b1;
                    biquad_equation_fsm_state <= MULT_A0_XN;
                end
            end

            MULT_A0_XN : begin
                mult_data_in_valid <= 1'b0;
                if (mult_result_valid == 1'b1) begin
                    adder_data_in_a <= mult_result;
                    adder_data_in_b <= aux;
                    adder_data_in_valid <= 1'b1;
                    biquad_equation_fsm_state <= ADD_YN;
                end
            end

            ADD_YN : begin
                adder_data_in_valid <= 1'b0;
                if (adder_result_valid == 1'b1) begin
                    o_done <= 1'b1;
                    o_yn <= adder_result;
                    biquad_equation_fsm_state <= IDLE;
                end
            end

            default : begin
                biquad_equation_fsm_state <= IDLE;
            end
        endcase
    end

endmodule
