module fpga_audio_processor_top (
    // Clock
    input   logic i_clock,      // 100 MHz
    // Input switches
    input   logic i_sw0,
    input   logic i_sw1,
    input   logic i_sw2,
    input   logic i_sw3,
    input   logic i_sw4,
    input   logic i_sw5,
    input   logic i_sw6,
    input   logic i_sw7,
    // Input buttons
    input   logic i_btnu,
    input   logic i_btnd,
    input   logic i_btnl,
    input   logic i_btnr,
    input   logic i_btnc,
    // Output LEDs
    output  logic o_ld0,
    output  logic o_ld1,
    output  logic o_ld2,
    output  logic o_ld3,
    output  logic o_ld4,
    output  logic o_ld5,
    output  logic o_ld6,
    output  logic o_ld7,
    // SPI Interface
    output  logic o_spi_cs_n,
    output  logic o_spi_clock,
    output  logic o_spi_mosi,
    input   logic i_spi_miso,
    // Audio Codec
    input   logic i_codec_bit_clock,
    input   logic i_codec_lr_clock,
    input   logic i_codec_adc_data,
    output  logic o_codec_mclock,
    output  logic o_codec_dac_data
);

    timeunit 1ns;
    timeprecision 1ps;

    parameter integer SPI_CLOCK_DIVIDER_WIDTH = 5;
    parameter integer SPI_DATA_WIDTH = 32;

    // Clock Generator
    logic clock_100MHz;
    clock_generator clock_generator_inst (
        .i_clock        (i_clock),
        .o_clock_100MHz (clock_100MHz),
        .o_clock_45MHz  (o_codec_mclock)
    );

    // Debouncer Core
    logic sw0;
    logic sw1;
    logic sw2;
    logic sw3;
    logic sw4;
    logic sw5;
    logic sw6;
    logic sw7;
    logic btnu;
    logic btnd;
    logic btnl;
    logic btnr;
    logic btnc;

    debouncer_zedboard # (
        .SWITCH_COUNT           (8),
        .BUTTON_COUNT           (5),
        .DEBOUNCE_COUNTER_WIDTH (16)
    ) debouncer_zedboard_inst (
        // Clock
        .i_clock                    (clock_100MHz),
        // Debounce counter values
        .i_switch_debounce_counter  (16'd10000),
        .i_button_debounce_counter  (16'd10000),
        // Input switches
        .i_sw0                      (i_sw0),
        .i_sw1                      (i_sw1),
        .i_sw2                      (i_sw2),
        .i_sw3                      (i_sw3),
        .i_sw4                      (i_sw4),
        .i_sw5                      (i_sw5),
        .i_sw6                      (i_sw6),
        .i_sw7                      (i_sw7),
        // Input buttons
        .i_btnu                     (i_btnu),
        .i_btnd                     (i_btnd),
        .i_btnl                     (i_btnl),
        .i_btnr                     (i_btnr),
        .i_btnc                     (i_btnc),
        // Debounced switch outputs
        .o_sw0                      (sw0),
        .o_sw1                      (sw1),
        .o_sw2                      (sw2),
        .o_sw3                      (sw3),
        .o_sw4                      (sw4),
        .o_sw5                      (sw5),
        .o_sw6                      (sw6),
        .o_sw7                      (sw7),
        // Debounced button outputs
        .o_btnu                     (btnu),
        .o_btnd                     (btnd),
        .o_btnl                     (btnl),
        .o_btnr                     (btnr),
        .o_btnc                     (btnc)
    );

    // SPI Controller
    spi_controller # (
        .SPI_CLOCK_DIVIDER_WIDTH    (SPI_CLOCK_DIVIDER_WIDTH),
        .SPI_DATA_WIDTH             (SPI_DATA_WIDTH)
    ) spi_controller_inst (
        // Clock, reset
        .i_clock        (clock_100MHz),
        .i_reset        (1'b0),
        // Control
        .i_enable       (btnc),
        // SPI interface
        .o_spi_cs_n     (o_spi_cs_n),
        .o_spi_clock    (o_spi_clock),
        .o_spi_mosi     (o_spi_mosi),
        .i_spi_miso     (i_spi_miso)
    );

    // Audio Processor
    audio_processor audio_processor_inst (
        .i_clock            (clock_100MHz),
        // Audio Interface
        .i_codec_bit_clock  (i_codec_bit_clock),
        .i_codec_lr_clock   (i_codec_lr_clock),
        .i_codec_adc_data   (i_codec_adc_data),
        .o_codec_dac_data   (o_codec_dac_data),
        // Switches
        .i_sw0              (sw0),
        .i_sw1              (sw1),
        .i_sw2              (sw2),
        .i_sw3              (sw3),
        .i_sw4              (sw4),
        .i_sw5              (sw5),
        .i_sw6              (sw6),
        .i_sw7              (sw7),
        // Buttons
        .i_btnu             (btnu),
        .i_btnd             (btnd),
        .i_btnl             (btnl),
        .i_btnr             (btnr),
        // LEDs
        .o_led              ({o_ld7, o_ld6, o_ld5, o_ld4, o_ld3, o_ld2, o_ld1, o_ld0})
    );

endmodule
