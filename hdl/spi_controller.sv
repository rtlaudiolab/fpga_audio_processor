module spi_controller # (
    parameter integer SPI_CLOCK_DIVIDER_WIDTH = 5,
    parameter integer SPI_DATA_WIDTH          = 32
) (
    // Clock, reset
    input   logic i_clock,
    input   logic i_reset,
    // Control
    input   logic i_enable,
    // SPI Interface
    output  logic o_spi_cs_n,
    output  logic o_spi_clock,
    output  logic o_spi_mosi,
    input   logic i_spi_miso
);

    timeunit 1ns;
    timeprecision 1ps;

    // SPI Driver <-> SPI Core connecting signals
    logic                           spi_enable;
    logic [SPI_DATA_WIDTH-1 : 0]    spi_data_in;
    logic [SPI_DATA_WIDTH-1 : 0]    spi_data_out;
    logic                           spi_done;
    logic                           spi_busy;

    // SPI Driver
    spi_driver # (
        .SPI_DATA_WIDTH (SPI_DATA_WIDTH) 
    ) spi_driver_inst (
        // Clock, reset
        .i_clock    (i_clock),
        .i_reset    (1'b0),
        // Control
        .i_enable   (i_enable),
        // SPI Master Control
        .i_data     (spi_data_out),
        .i_done     (spi_done),
        .i_busy     (spi_busy),
        .o_enable   (spi_enable),
        .o_data     (spi_data_in)
    );

    // SPI Core
    spi_master # (
        .SPI_CLOCK_DIVIDER_WIDTH    (SPI_CLOCK_DIVIDER_WIDTH),
        .SPI_DATA_WIDTH             (SPI_DATA_WIDTH)
    ) spi_master_inst (
        // Clock, reset
        .i_clock                (i_clock),
        .i_reset                (1'b0),
        // Data, control and status interface
        .i_enable               (spi_enable),
        .i_clock_polarity       (1'b0),
        .i_clock_phase          (1'b0),
        .i_spi_clock_divider    (5'b10000),
        .i_data_in              (spi_data_in),
        .o_data_out             (spi_data_out),
        .o_done                 (spi_done),
        .o_busy                 (spi_busy),
        // SPI interface
        .o_spi_cs_n             (o_spi_cs_n),
        .o_spi_clock            (o_spi_clock),
        .o_spi_mosi             (o_spi_mosi),
        .i_spi_miso             (i_spi_miso)
    );

endmodule
