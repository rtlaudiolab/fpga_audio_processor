module fixed_to_float_converter # (
    parameter integer FIXED_POINT_BIT_WIDTH = 24,
    parameter integer FLOATING_POINT_BIT_WIDTH = 32
    ) (
    input   logic                                       i_clock,
    // Audio Input
    input   logic                                       i_data_valid,
    input   logic   [FIXED_POINT_BIT_WIDTH-1 : 0]       i_data_left,
    input   logic   [FIXED_POINT_BIT_WIDTH-1 : 0]       i_data_right,
    // Audio Output
    output  logic                                       o_data_valid,
    output  logic   [FLOATING_POINT_BIT_WIDTH-1 : 0]    o_data_left,
    output  logic   [FLOATING_POINT_BIT_WIDTH-1 : 0]    o_data_right
);

    timeunit 1ns;
    timeprecision 1ps;

    // Fixed-to-float Conversion
    logic                                   fixed_to_float_valid_in;
    logic [FIXED_POINT_BIT_WIDTH-1 : 0]     fixed_to_float_data_in;
    logic                                   fixed_to_float_valid_out;
    logic [FLOATING_POINT_BIT_WIDTH-1 : 0]  fixed_to_float_data_out;
    fixed_to_float fixed_to_float_inst (
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (fixed_to_float_valid_in),
        .s_axis_a_tdata         (fixed_to_float_data_in),
        .m_axis_result_tvalid   (fixed_to_float_valid_out),
        .m_axis_result_tdata    (fixed_to_float_data_out)
    );

    // Fixed to Float FSM
    enum logic [1 : 0]  {IDLE,
                        CONVERT_LEFT_CHANNEL,
                        CONVERT_RIGHT_CHANNEL} fixed_to_float_fsm_state = IDLE;
    logic [FIXED_POINT_BIT_WIDTH-1 : 0] data_right = 'b0;

    always_ff @(posedge i_clock) begin : fixed_to_float_fsm
        case (fixed_to_float_fsm_state)
            IDLE : begin
                o_data_valid <= 1'b0;
                fixed_to_float_valid_in <= 1'b0;
                if (i_data_valid == 1'b1) begin
                    data_right <= i_data_right;
                    fixed_to_float_valid_in <= 1'b1;
                    fixed_to_float_data_in <= i_data_left;
                    fixed_to_float_fsm_state <= CONVERT_LEFT_CHANNEL;
                end
            end

            CONVERT_LEFT_CHANNEL : begin
                fixed_to_float_valid_in <= 1'b0;
                if (fixed_to_float_valid_out == 1'b1) begin
                    o_data_left <= fixed_to_float_data_out;
                    fixed_to_float_valid_in <= 1'b1;
                    fixed_to_float_data_in <= data_right;
                    fixed_to_float_fsm_state <= CONVERT_RIGHT_CHANNEL;
                end
            end

            CONVERT_RIGHT_CHANNEL : begin
                fixed_to_float_valid_in <= 1'b0;
                if (fixed_to_float_valid_out == 1'b1) begin
                    o_data_right <= fixed_to_float_data_out;
                    o_data_valid <= 1'b1;
                    fixed_to_float_fsm_state <= IDLE;
                end
            end

            default : begin
                fixed_to_float_fsm_state <= IDLE;
            end
        endcase
    end

endmodule
