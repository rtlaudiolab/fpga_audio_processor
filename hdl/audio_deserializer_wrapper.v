module audio_deserializer_wrapper ( 
    input   wire           i_clock,
    // I2S Interface
    input   wire           i_codec_bit_clock,
    input   wire           i_codec_lr_clock,
    input   wire           i_codec_adc_data,
    // Parallel Data Output
    output  wire [23 : 0]  o_data_left,
    output  wire [23 : 0]  o_data_right,
    output  wire           o_data_valid
);

    audio_deserializer audio_deserializer_inst (
    .i_clock            (i_clock),
    .i_codec_bit_clock  (i_codec_bit_clock),
    .i_codec_lr_clock   (i_codec_lr_clock),
    .i_codec_adc_data   (i_codec_adc_data),
    .o_data_left        (o_data_left),
    .o_data_right       (o_data_right),
    .o_data_valid       (o_data_valid)
    );

endmodule
