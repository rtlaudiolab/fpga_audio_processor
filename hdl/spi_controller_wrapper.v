module spi_controller_wrapper # (
    parameter integer SPI_CLOCK_DIVIDER_WIDTH = 5,
    parameter integer SPI_DATA_WIDTH          = 32
) (
    // Clock, reset
    input   wire i_clock,
    input   wire i_reset,
    // Control
    input   wire i_enable,
    // SPI Interface
    output  wire o_spi_cs_n,
    output  wire o_spi_clock,
    output  wire o_spi_mosi,
    input   wire i_spi_miso
);

    spi_controller # (
        .SPI_CLOCK_DIVIDER_WIDTH    (SPI_CLOCK_DIVIDER_WIDTH),
        .SPI_DATA_WIDTH             (SPI_DATA_WIDTH)
    ) spi_controller_inst (
        // Clock, reset
        .i_clock        (i_clock),
        .i_reset        (i_reset),
        // Control
        .i_enable       (i_enable),
        // SPI Interface
        .o_spi_cs_n     (o_spi_cs_n),
        .o_spi_clock    (o_spi_clock),
        .o_spi_mosi     (o_spi_mosi),
        .i_spi_miso     (i_spi_miso)
    );

endmodule
