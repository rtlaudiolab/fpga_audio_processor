module led_meter_fsm (
    input   logic i_clock,
    // Audio Input
    input   logic signed    [23 : 0]    i_data,
    input   logic                       i_data_valid,
    // LED Meter Output
    output  logic           [3 : 0]     o_led
);

    timeunit 1ns;
    timeprecision 1ps;

    logic [3 : 0] led;
    logic signed [23 : 0] audio_sample;
    logic [27 : 0] overflow_counter;

    // Main FSM
    enum logic [1 : 0]  {IDLE,
                        GET_ABS_VALUE,
                        UPDATE_LED,
                        OVERFLOW} main_fsm_state = IDLE;

    always_ff @(posedge i_clock) begin : main_fsm
        case (main_fsm_state)
            IDLE : begin
                overflow_counter <= 'b0;
                if (i_data_valid == 1'b1) begin
                    audio_sample <= i_data;
                    main_fsm_state <= GET_ABS_VALUE;
                end
            end

            GET_ABS_VALUE : begin
                if (audio_sample[$left(audio_sample)] == 1'b1) begin      // Negative value, need to convert to positive
                    audio_sample <= (~audio_sample) + 1;
                end
                main_fsm_state <= UPDATE_LED;
            end

            UPDATE_LED : begin
                main_fsm_state <= IDLE;
                if (audio_sample > 7919356) begin  // ~-0.5 dBFS
                    main_fsm_state <= OVERFLOW;
                    led <= 'b1111;
                end else if (audio_sample > 4194304) begin  // ~-6 dBFS
                    led <= 4'b1111;
                end else if (audio_sample > 2097152) begin  // ~-12 dBFS
                    led <= 4'b0111;
                end else if (audio_sample > 1048576) begin  // ~-24 dBFS
                    led <= 4'b0011;
                end else if (audio_sample > 524288) begin   // ~-48 dBFS
                    led <= 4'b0001;
                end else begin
                    led <= 4'b0000;
                end
            end

            OVERFLOW : begin
                overflow_counter <= overflow_counter + 1;
                if  ((overflow_counter == 20000000) ||
                    (overflow_counter == 40000000) ||
                    (overflow_counter == 60000000) ||
                    (overflow_counter == 80000000) ||
                    (overflow_counter == 100000000) ||
                    (overflow_counter == 120000000)) begin
                    led <= ~led;
                end
                if (overflow_counter == 140000000) begin
                    led <= 'b0;
                    main_fsm_state <= IDLE;
                end
            end

            default : begin
                main_fsm_state <= IDLE;
            end
        endcase
    end
    assign o_led = led;

endmodule
