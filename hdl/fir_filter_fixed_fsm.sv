module fir_filter_fixed_fsm (
    input   logic                   i_clock,
    // Audio Input
    input   logic                   i_data_valid,
    input   logic signed [23 : 0]   i_data,
    // Audio Output
    output  logic                   o_data_valid,
    output  logic signed [23 : 0]   o_data
);

    timeunit 1ns;
    timeprecision 1ps;

    logic [4 : 0]   fir_filter_fixed_sample_dram_wr_addr;
    logic [4 : 0]   fir_filter_fixed_sample_dram_rd_addr;
    logic signed [23 : 0]  fir_filter_fixed_sample_dram_data_out;
    fir_filter_fixed_sample_dram fir_filter_fixed_sample_dram_inst (
        .a          (fir_filter_fixed_sample_dram_wr_addr),
        .d          (i_data),
        .dpra       (fir_filter_fixed_sample_dram_rd_addr),
        .clk        (i_clock),
        .we         (i_data_valid),
        .qdpo       (fir_filter_fixed_sample_dram_data_out)
    );

    logic [4 : 0]   fir_filter_fixed_coeff_drom_rd_addr;
    logic signed [31 : 0]  fir_filter_fixed_coeff_drom_data_out;
    fir_filter_fixed_coeff_drom fir_filter_fixed_coeff_drom_inst (
        .a          (fir_filter_fixed_coeff_drom_rd_addr),
        .clk        (i_clock),
        .qspo       (fir_filter_fixed_coeff_drom_data_out)
    );

    logic signed [23 : 0] mult_data_in_a;
    logic signed [31 : 0] mult_data_in_b;
    logic signed [55 : 0] mult_data_out;
    fixed_point_multiplier fixed_point_multiplier_inst (
        .CLK  (i_clock),
        .A    (mult_data_in_a),
        .B    (mult_data_in_b),
        .P    (mult_data_out)
    );

    // FIR Filter FSM
    typedef enum logic [2 : 0]  {IDLE,
                                WAIT_SAMPLE,
                                MULTIPLY,
                                WAIT_MULTIPLICATION,
                                ACCUMULATE,
                                UPDATE_OUTPUT} fir_filter_fixed_fsm_t;
    fir_filter_fixed_fsm_t fir_filter_fixed_fsm_state = IDLE;
    logic [4 : 0] mac_counter;
    logic sample_buffer_full;
    logic mac_busy;
    logic signed [60 : 0] accumulator;

    always_ff @(posedge i_clock) begin : fir_filter_fixed_fsm
        case (fir_filter_fixed_fsm_state)

            IDLE : begin
                mult_data_in_a <= 24'd0;
                mult_data_in_b <= 32'd0;
                fir_filter_fixed_sample_dram_wr_addr <= 5'd0;
                fir_filter_fixed_sample_dram_rd_addr <= 5'b0;
                fir_filter_fixed_coeff_drom_rd_addr <= 5'b0;
                mac_counter <= 5'd0;
                sample_buffer_full <= 1'b0;
                mac_busy <= 1'b0;
                o_data_valid <= 1'b0;
                o_data <= 32'd0;
                accumulator <= 61'b0;
                fir_filter_fixed_fsm_state <= WAIT_SAMPLE;
            end

            WAIT_SAMPLE : begin
                o_data_valid <= 1'b0;
                if (sample_buffer_full == 1'b0) begin
                    if (i_data_valid == 1'b1) begin
                        fir_filter_fixed_sample_dram_wr_addr <= fir_filter_fixed_sample_dram_wr_addr + 1;
                        mac_counter <=  mac_counter + 1;
                    end
                    if (mac_counter == 5'd30) begin
                        sample_buffer_full <= 1'b1;
                    end
                end else begin
                    if (i_data_valid == 1'b1) begin
                        fir_filter_fixed_sample_dram_wr_addr <= fir_filter_fixed_sample_dram_wr_addr + 1;
                        fir_filter_fixed_sample_dram_rd_addr <= fir_filter_fixed_sample_dram_wr_addr + 1;
                        fir_filter_fixed_coeff_drom_rd_addr <= 5'd0;
                        mac_counter <= 5'd0;
                        fir_filter_fixed_fsm_state <= MULTIPLY;
                    end
                end
            end

            MULTIPLY : begin
                if (mac_counter == 5'd30) begin
                    mac_counter <= 5'd0;
                    fir_filter_fixed_fsm_state <= UPDATE_OUTPUT;
                end else begin
                    mult_data_in_a <= fir_filter_fixed_sample_dram_data_out;
                    mult_data_in_b <= fir_filter_fixed_coeff_drom_data_out;
                    fir_filter_fixed_fsm_state <= WAIT_MULTIPLICATION;
                end
            end

            WAIT_MULTIPLICATION : begin
                fir_filter_fixed_fsm_state <= ACCUMULATE;
                fir_filter_fixed_sample_dram_rd_addr <= fir_filter_fixed_sample_dram_rd_addr + 1;
                fir_filter_fixed_coeff_drom_rd_addr <= fir_filter_fixed_coeff_drom_rd_addr + 1;
            end

            ACCUMULATE : begin
                    mac_counter <= mac_counter + 1;
                    accumulator <= accumulator + mult_data_out;
                    fir_filter_fixed_fsm_state <= MULTIPLY;
            end

            UPDATE_OUTPUT : begin
                accumulator <= 115'd0;
                o_data_valid <= 1'b1;
                o_data <= accumulator[60 : 36];
                fir_filter_fixed_fsm_state <= WAIT_SAMPLE;
            end

            default : begin
                fir_filter_fixed_fsm_state <= IDLE;
            end
        endcase
    end

endmodule
