module single_to_double_converter # (
    parameter integer SP_FLOATING_POINT_BIT_WIDTH = 32,
    parameter integer DP_FLOATING_POINT_BIT_WIDTH = 64
    ) (
    input   logic                                       i_clock,
    // Audio Input
    input   logic                                       i_data_valid,
    input   logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_data_left,
    input   logic   [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] i_data_right,
    // Audio Output
    output  logic                                       o_data_valid,
    output  logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] o_data_left,
    output  logic   [DP_FLOATING_POINT_BIT_WIDTH-1 : 0] o_data_right
);

    timeunit 1ns;
    timeprecision 1ps;

    // Single-to-double Conversion
    logic                                       single_to_double_valid_in;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0]   single_to_double_data_in;
    logic                                       single_to_double_valid_out;
    logic [DP_FLOATING_POINT_BIT_WIDTH-1 : 0]   single_to_double_data_out;
    single_to_double single_to_double_inst (
        .aclk                   (i_clock),
        .s_axis_a_tvalid        (single_to_double_valid_in),
        .s_axis_a_tdata         (single_to_double_data_in),
        .m_axis_result_tvalid   (single_to_double_valid_out),
        .m_axis_result_tdata    (single_to_double_data_out)
    );

    // Single to Double FSM
    enum logic [1 : 0]  {IDLE,
                        CONVERT_LEFT_CHANNEL,
                        CONVERT_RIGHT_CHANNEL} single_to_double_fsm_state = IDLE;
    logic [SP_FLOATING_POINT_BIT_WIDTH-1 : 0] data_right = 'b0;

    always_ff @(posedge i_clock) begin : single_to_double_fsm
        case (single_to_double_fsm_state)
            IDLE : begin
                o_data_valid <= 1'b0;
                single_to_double_valid_in <= 1'b0;
                if (i_data_valid == 1'b1) begin
                    data_right <= i_data_right;
                    single_to_double_valid_in <= 1'b1;
                    single_to_double_data_in <= i_data_left;
                    single_to_double_fsm_state <= CONVERT_LEFT_CHANNEL;
                end
            end

            CONVERT_LEFT_CHANNEL : begin
                single_to_double_valid_in <= 1'b0;
                if (single_to_double_valid_out == 1'b1) begin
                    o_data_left <= single_to_double_data_out;
                    single_to_double_valid_in <= 1'b1;
                    single_to_double_data_in <= data_right;
                    single_to_double_fsm_state <= CONVERT_RIGHT_CHANNEL;
                end
            end

            CONVERT_RIGHT_CHANNEL : begin
                single_to_double_valid_in <= 1'b0;
                if (single_to_double_valid_out == 1'b1) begin
                    o_data_right <= single_to_double_data_out;
                    o_data_valid <= 1'b1;
                    single_to_double_fsm_state <= IDLE;
                end
            end

            default : begin
                single_to_double_fsm_state <= IDLE;
            end
        endcase
    end

endmodule
