# Step 1: define output directory
set output_folder ../output

# Step 2: create in-memory project and define target part
create_project -in_memory
set_property BOARD_PART em.avnet.com:zed:part0:1.4 [current_project]

# Step 3: read design sources and IP files
read_verilog -sv    ../hdl/debounce_fsm.sv
read_verilog -sv    ../hdl/debouncer_zedboard.sv
read_verilog -sv    ../hdl/spi_driver.sv
read_verilog -sv    ../hdl/spi_master.sv
read_verilog -sv    ../hdl/spi_controller.sv
read_ip             ../ip/oddr_0/oddr_0.xci
generate_target {synthesis} [get_files  ../ip/oddr_0/oddr_0.xci]
read_ip             ../ip/clk_wiz_0/clk_wiz_0.xci
generate_target {synthesis} [get_files  ../ip/clk_wiz_0/clk_wiz_0.xci]
read_verilog -sv    ../hdl/clock_generator.sv
read_verilog -sv    ../hdl/audio_deserializer.sv
read_verilog -sv    ../hdl/led_meter_fsm.sv
read_verilog -sv    ../hdl/led_meter.sv
read_ip             ../ip/fir_filter_fixed_coeff_drom/fir_filter_fixed_coeff_drom.xci
generate_target {synthesis} [get_files  ../ip/fir_filter_fixed_coeff_drom/fir_filter_fixed_coeff_drom.xci]
# read_vhdl           ../ip/fir_filter_fixed_coeff_drom/synth/fir_filter_fixed_coeff_drom.vhd
read_ip             ../ip/fir_filter_fixed_sample_dram/fir_filter_fixed_sample_dram.xci
generate_target {synthesis} [get_files  ../ip/fir_filter_fixed_sample_dram/fir_filter_fixed_sample_dram.xci]
# read_vhdl           ../ip/fir_filter_fixed_sample_dram/synth/fir_filter_fixed_sample_dram.vhd
read_ip             ../ip/fixed_point_multiplier/fixed_point_multiplier.xci
generate_target {synthesis} [get_files  ../ip/fixed_point_multiplier/fixed_point_multiplier.xci]
# read_vhdl           ../ip/fixed_point_multiplier/synth/fixed_point_multiplier.vhd
read_verilog -sv    ../hdl/fir_filter_fixed_fsm.sv
read_verilog -sv    ../hdl/fir_filter_fixed.sv
read_verilog -sv    ../hdl/audio_processor.sv
read_verilog -sv    ../hdl/audio_serializer.sv
read_verilog -sv    ../hdl/fpga_audio_processor_top.sv

# Step 4: read synthesis constraints
read_xdc ../constraints/synthesis.xdc

# Step 5: run synthesis, report utilization and timing estimates, write post-synthesis design checkpoint
synth_design -top fpga_audio_processor_top
report_timing_summary -file $output_folder/synthesis/post_synth_timing_summary.rpt
report_power -file $output_folder/synthesis/post_synth_power.rpt
write_checkpoint -force $output_folder/synthesis/post_synthesis_design_checkpoint

# Step 6: read implementation constraints
read_xdc ../constraints/implementation.xdc
 
# Step 7: run placer and logic optimzation, report utilization and timing estimates, write post-place design checkpoint
opt_design
place_design
phys_opt_design
report_timing_summary -file $output_folder/place/post_place_timing_summary.rpt
write_checkpoint -force $output_folder/place/post_place_design_checkpoint
 
# Step 8: run router, report final utilization and timing , run drc, write post-route design checkpoint
route_design
report_timing_summary -file $output_folder/route/post_route_timing_summary.rpt
report_timing -sort_by group -max_paths 100 -path_type summary -file $output_folder/route/post_route_timing.rpt
report_clock_utilization -file $output_folder/route/clock_util.rpt
report_utilization -file $output_folder/route/post_route_util.rpt
report_power -file $output_folder/route/post_route_power.rpt
report_drc -file $output_folder/route/post_route_drc.rpt
write_checkpoint -force $output_folder/route/post_route_design_checkpoint

# Step 9: generate bitstream
write_bitstream -force $output_folder/bitstream/fpga_audio_processor.bit

# Step 10: program bitstream
open_hw_manager
connect_hw_server -allow_non_jtag
open_hw_target
current_hw_device [get_hw_devices xc7z020_1]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xc7z020_1] 0]
set_property PROBES.FILE {} [get_hw_devices xc7z020_1]
set_property FULL_PROBES.FILE {} [get_hw_devices xc7z020_1]
set_property PROGRAM.FILE {../output/bitstream/fpga_audio_processor.bit} [get_hw_devices xc7z020_1]
program_hw_devices [get_hw_devices xc7z020_1]
refresh_hw_device [lindex [get_hw_devices xc7z020_1] 0]
close_hw_target
