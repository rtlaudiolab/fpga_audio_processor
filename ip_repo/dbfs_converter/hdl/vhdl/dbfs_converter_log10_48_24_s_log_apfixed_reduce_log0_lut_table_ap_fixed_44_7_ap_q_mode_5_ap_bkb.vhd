-- ==============================================================
-- Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- ==============================================================
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;

entity dbfs_converter_log10_48_24_s_log_apfixed_reduce_log0_lut_table_ap_fixed_44_7_ap_q_mode_5_ap_bkb_rom is 
    generic(
             DWIDTH     : integer := 37; 
             AWIDTH     : integer := 6; 
             MEM_SIZE    : integer := 64
    ); 
    port (
          addr0      : in std_logic_vector(AWIDTH-1 downto 0); 
          ce0       : in std_logic; 
          q0         : out std_logic_vector(DWIDTH-1 downto 0);
          clk       : in std_logic
    ); 
end entity; 


architecture rtl of dbfs_converter_log10_48_24_s_log_apfixed_reduce_log0_lut_table_ap_fixed_44_7_ap_q_mode_5_ap_bkb_rom is 

signal addr0_tmp : std_logic_vector(AWIDTH-1 downto 0); 
type mem_array is array (0 to MEM_SIZE-1) of std_logic_vector (DWIDTH-1 downto 0); 
signal mem : mem_array := (
    0 to 2=> "1111111101111100000111111111110101011", 
    3 to 4=> "0000011110011100110011101100001001001", 
    5 to 6=> "0001000000000001101110001011001011110", 
    7 to 9=> "0001100010101111011111100101101010110", 
    10 to 11=> "0010000110101011001111010000000110101", 
    12 to 14=> "0010101011111010101000001101010000000", 
    15 to 17=> "0011010010100011111110100111011001101", 
    18 to 21=> "0011111010101110010110001101011011000", 
    22 to 25=> "0100100100100001101010000100001000101", 
    26 to 29=> "0101010000000110110110000001101000111", 
    30 to 31=> "0101111101101000000010001110110010111", 
    32 to 33=> "1011001111011000100101010001000111010", 
    34 to 35=> "1011100111011110101011010100111010101", 
    36 to 38=> "1100000000001001111011110010001010011", 
    39 to 41=> "1100011001011100001100000110111000100", 
    42 to 43=> "1100110011010111011010101100001101110", 
    44 to 46=> "1101001101111101101111110001110000001", 
    47 to 49=> "1101101001010001011110100000110100011", 
    50 to 53=> "1110000101010101000110001000111100101", 
    54 to 56=> "1110100010001011010011010111001001110", 
    57 to 60=> "1110111111110111000001111001110101010", 
    61 to 63=> "1111011110011011011110010011100110111" );


begin 


memory_access_guard_0: process (addr0) 
begin
      addr0_tmp <= addr0;
--synthesis translate_off
      if (CONV_INTEGER(addr0) > mem_size-1) then
           addr0_tmp <= (others => '0');
      else 
           addr0_tmp <= addr0;
      end if;
--synthesis translate_on
end process;

p_rom_access: process (clk)  
begin 
    if (clk'event and clk = '1') then
        if (ce0 = '1') then 
            q0 <= mem(CONV_INTEGER(addr0_tmp)); 
        end if;
    end if;
end process;

end rtl;

Library IEEE;
use IEEE.std_logic_1164.all;

entity dbfs_converter_log10_48_24_s_log_apfixed_reduce_log0_lut_table_ap_fixed_44_7_ap_q_mode_5_ap_bkb is
    generic (
        DataWidth : INTEGER := 37;
        AddressRange : INTEGER := 64;
        AddressWidth : INTEGER := 6);
    port (
        reset : IN STD_LOGIC;
        clk : IN STD_LOGIC;
        address0 : IN STD_LOGIC_VECTOR(AddressWidth - 1 DOWNTO 0);
        ce0 : IN STD_LOGIC;
        q0 : OUT STD_LOGIC_VECTOR(DataWidth - 1 DOWNTO 0));
end entity;

architecture arch of dbfs_converter_log10_48_24_s_log_apfixed_reduce_log0_lut_table_ap_fixed_44_7_ap_q_mode_5_ap_bkb is
    component dbfs_converter_log10_48_24_s_log_apfixed_reduce_log0_lut_table_ap_fixed_44_7_ap_q_mode_5_ap_bkb_rom is
        port (
            clk : IN STD_LOGIC;
            addr0 : IN STD_LOGIC_VECTOR;
            ce0 : IN STD_LOGIC;
            q0 : OUT STD_LOGIC_VECTOR);
    end component;



begin
    dbfs_converter_log10_48_24_s_log_apfixed_reduce_log0_lut_table_ap_fixed_44_7_ap_q_mode_5_ap_bkb_rom_U :  component dbfs_converter_log10_48_24_s_log_apfixed_reduce_log0_lut_table_ap_fixed_44_7_ap_q_mode_5_ap_bkb_rom
    port map (
        clk => clk,
        addr0 => address0,
        ce0 => ce0,
        q0 => q0);

end architecture;


