// ==============================================================
// RTL generated by Vitis HLS - High-Level Synthesis from C, C++ and OpenCL v2020.2 (64-bit)
// Version: 2020.2
// Copyright (C) Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

(* CORE_GENERATION_INFO="dbfs_converter_dbfs_converter,hls_ip_2020_2,{HLS_INPUT_TYPE=cxx,HLS_INPUT_FLOAT=0,HLS_INPUT_FIXED=0,HLS_INPUT_PART=xc7z020-clg484-1,HLS_INPUT_CLOCK=10.000000,HLS_INPUT_ARCH=others,HLS_SYN_CLOCK=7.080000,HLS_SYN_LAT=24,HLS_SYN_TPT=none,HLS_SYN_MEM=3,HLS_SYN_DSP=0,HLS_SYN_FF=3350,HLS_SYN_LUT=2995,HLS_VERSION=2020_2}" *)

module dbfs_converter (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_idle,
        ap_ready,
        linear_value,
        ap_return
);

parameter    ap_ST_fsm_state1 = 25'd1;
parameter    ap_ST_fsm_state2 = 25'd2;
parameter    ap_ST_fsm_state3 = 25'd4;
parameter    ap_ST_fsm_state4 = 25'd8;
parameter    ap_ST_fsm_state5 = 25'd16;
parameter    ap_ST_fsm_state6 = 25'd32;
parameter    ap_ST_fsm_state7 = 25'd64;
parameter    ap_ST_fsm_state8 = 25'd128;
parameter    ap_ST_fsm_state9 = 25'd256;
parameter    ap_ST_fsm_state10 = 25'd512;
parameter    ap_ST_fsm_state11 = 25'd1024;
parameter    ap_ST_fsm_state12 = 25'd2048;
parameter    ap_ST_fsm_state13 = 25'd4096;
parameter    ap_ST_fsm_state14 = 25'd8192;
parameter    ap_ST_fsm_state15 = 25'd16384;
parameter    ap_ST_fsm_state16 = 25'd32768;
parameter    ap_ST_fsm_state17 = 25'd65536;
parameter    ap_ST_fsm_state18 = 25'd131072;
parameter    ap_ST_fsm_state19 = 25'd262144;
parameter    ap_ST_fsm_state20 = 25'd524288;
parameter    ap_ST_fsm_state21 = 25'd1048576;
parameter    ap_ST_fsm_state22 = 25'd2097152;
parameter    ap_ST_fsm_state23 = 25'd4194304;
parameter    ap_ST_fsm_state24 = 25'd8388608;
parameter    ap_ST_fsm_state25 = 25'd16777216;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
output   ap_idle;
output   ap_ready;
input  [47:0] linear_value;
output  [47:0] ap_return;

reg ap_done;
reg ap_idle;
reg ap_ready;

(* fsm_encoding = "none" *) reg   [24:0] ap_CS_fsm;
wire    ap_CS_fsm_state1;
wire   [47:0] select_ln180_fu_105_p3;
reg   [47:0] select_ln180_reg_202;
reg   [0:0] tmp_74_reg_207;
reg   [24:0] trunc_ln1148_2_reg_212;
wire   [25:0] x_V_fu_167_p3;
reg   [25:0] x_V_reg_217;
wire    ap_CS_fsm_state2;
wire   [27:0] grp_log10_48_24_s_fu_68_ap_return;
reg   [27:0] op_V_reg_222;
wire    ap_CS_fsm_state24;
wire    grp_log10_48_24_s_fu_68_ap_start;
wire    grp_log10_48_24_s_fu_68_ap_done;
wire    grp_log10_48_24_s_fu_68_ap_idle;
wire    grp_log10_48_24_s_fu_68_ap_ready;
reg    grp_log10_48_24_s_fu_68_ap_start_reg;
reg   [24:0] ap_NS_fsm;
wire    ap_NS_fsm_state3;
wire    ap_CS_fsm_state3;
wire   [47:0] p_Val2_s_fu_81_p2;
wire   [0:0] p_Result_s_fu_97_p3;
wire   [47:0] p_Result_3_fu_87_p4;
wire   [71:0] t_fu_131_p3;
wire   [71:0] sub_ln1148_fu_138_p2;
wire   [24:0] tmp_fu_144_p4;
wire   [25:0] zext_ln1148_1_fu_154_p1;
wire   [25:0] sub_ln1148_1_fu_161_p2;
wire   [25:0] zext_ln1148_fu_158_p1;
wire    ap_CS_fsm_state25;
wire   [31:0] tmp_75_fu_174_p3;
wire   [29:0] tmp_76_fu_185_p3;
wire  signed [47:0] sext_ln703_fu_181_p1;
wire  signed [47:0] sext_ln703_1_fu_192_p1;
wire    ap_ce_reg;

// power-on initialization
initial begin
#0 ap_CS_fsm = 25'd1;
#0 grp_log10_48_24_s_fu_68_ap_start_reg = 1'b0;
end

dbfs_converter_log10_48_24_s grp_log10_48_24_s_fu_68(
    .ap_clk(ap_clk),
    .ap_rst(ap_rst),
    .ap_start(grp_log10_48_24_s_fu_68_ap_start),
    .ap_done(grp_log10_48_24_s_fu_68_ap_done),
    .ap_idle(grp_log10_48_24_s_fu_68_ap_idle),
    .ap_ready(grp_log10_48_24_s_fu_68_ap_ready),
    .x(x_V_reg_217),
    .ap_return(grp_log10_48_24_s_fu_68_ap_return)
);

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_fsm_state1;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        grp_log10_48_24_s_fu_68_ap_start_reg <= 1'b0;
    end else begin
        if (((1'b1 == ap_NS_fsm_state3) & (1'b1 == ap_CS_fsm_state2))) begin
            grp_log10_48_24_s_fu_68_ap_start_reg <= 1'b1;
        end else if ((grp_log10_48_24_s_fu_68_ap_ready == 1'b1)) begin
            grp_log10_48_24_s_fu_68_ap_start_reg <= 1'b0;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_CS_fsm_state24)) begin
        op_V_reg_222 <= grp_log10_48_24_s_fu_68_ap_return;
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_CS_fsm_state1)) begin
        select_ln180_reg_202 <= select_ln180_fu_105_p3;
        tmp_74_reg_207 <= select_ln180_fu_105_p3[32'd47];
        trunc_ln1148_2_reg_212 <= {{select_ln180_fu_105_p3[47:23]}};
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_CS_fsm_state2)) begin
        x_V_reg_217 <= x_V_fu_167_p3;
    end
end

always @ (*) begin
    if ((1'b1 == ap_CS_fsm_state25)) begin
        ap_done = 1'b1;
    end else begin
        ap_done = 1'b0;
    end
end

always @ (*) begin
    if (((ap_start == 1'b0) & (1'b1 == ap_CS_fsm_state1))) begin
        ap_idle = 1'b1;
    end else begin
        ap_idle = 1'b0;
    end
end

always @ (*) begin
    if ((1'b1 == ap_CS_fsm_state25)) begin
        ap_ready = 1'b1;
    end else begin
        ap_ready = 1'b0;
    end
end

always @ (*) begin
    case (ap_CS_fsm)
        ap_ST_fsm_state1 : begin
            if (((ap_start == 1'b1) & (1'b1 == ap_CS_fsm_state1))) begin
                ap_NS_fsm = ap_ST_fsm_state2;
            end else begin
                ap_NS_fsm = ap_ST_fsm_state1;
            end
        end
        ap_ST_fsm_state2 : begin
            ap_NS_fsm = ap_ST_fsm_state3;
        end
        ap_ST_fsm_state3 : begin
            ap_NS_fsm = ap_ST_fsm_state4;
        end
        ap_ST_fsm_state4 : begin
            ap_NS_fsm = ap_ST_fsm_state5;
        end
        ap_ST_fsm_state5 : begin
            ap_NS_fsm = ap_ST_fsm_state6;
        end
        ap_ST_fsm_state6 : begin
            ap_NS_fsm = ap_ST_fsm_state7;
        end
        ap_ST_fsm_state7 : begin
            ap_NS_fsm = ap_ST_fsm_state8;
        end
        ap_ST_fsm_state8 : begin
            ap_NS_fsm = ap_ST_fsm_state9;
        end
        ap_ST_fsm_state9 : begin
            ap_NS_fsm = ap_ST_fsm_state10;
        end
        ap_ST_fsm_state10 : begin
            ap_NS_fsm = ap_ST_fsm_state11;
        end
        ap_ST_fsm_state11 : begin
            ap_NS_fsm = ap_ST_fsm_state12;
        end
        ap_ST_fsm_state12 : begin
            ap_NS_fsm = ap_ST_fsm_state13;
        end
        ap_ST_fsm_state13 : begin
            ap_NS_fsm = ap_ST_fsm_state14;
        end
        ap_ST_fsm_state14 : begin
            ap_NS_fsm = ap_ST_fsm_state15;
        end
        ap_ST_fsm_state15 : begin
            ap_NS_fsm = ap_ST_fsm_state16;
        end
        ap_ST_fsm_state16 : begin
            ap_NS_fsm = ap_ST_fsm_state17;
        end
        ap_ST_fsm_state17 : begin
            ap_NS_fsm = ap_ST_fsm_state18;
        end
        ap_ST_fsm_state18 : begin
            ap_NS_fsm = ap_ST_fsm_state19;
        end
        ap_ST_fsm_state19 : begin
            ap_NS_fsm = ap_ST_fsm_state20;
        end
        ap_ST_fsm_state20 : begin
            ap_NS_fsm = ap_ST_fsm_state21;
        end
        ap_ST_fsm_state21 : begin
            ap_NS_fsm = ap_ST_fsm_state22;
        end
        ap_ST_fsm_state22 : begin
            ap_NS_fsm = ap_ST_fsm_state23;
        end
        ap_ST_fsm_state23 : begin
            ap_NS_fsm = ap_ST_fsm_state24;
        end
        ap_ST_fsm_state24 : begin
            ap_NS_fsm = ap_ST_fsm_state25;
        end
        ap_ST_fsm_state25 : begin
            ap_NS_fsm = ap_ST_fsm_state1;
        end
        default : begin
            ap_NS_fsm = 'bx;
        end
    endcase
end

assign ap_CS_fsm_state1 = ap_CS_fsm[32'd0];

assign ap_CS_fsm_state2 = ap_CS_fsm[32'd1];

assign ap_CS_fsm_state24 = ap_CS_fsm[32'd23];

assign ap_CS_fsm_state25 = ap_CS_fsm[32'd24];

assign ap_CS_fsm_state3 = ap_CS_fsm[32'd2];

assign ap_NS_fsm_state3 = ap_NS_fsm[32'd2];

assign ap_return = ($signed(sext_ln703_fu_181_p1) + $signed(sext_ln703_1_fu_192_p1));

assign grp_log10_48_24_s_fu_68_ap_start = grp_log10_48_24_s_fu_68_ap_start_reg;

assign p_Result_3_fu_87_p4 = {|(1'd0), p_Val2_s_fu_81_p2[47 - 1:0]};

assign p_Result_s_fu_97_p3 = linear_value[32'd47];

assign p_Val2_s_fu_81_p2 = (48'd0 - linear_value);

assign select_ln180_fu_105_p3 = ((p_Result_s_fu_97_p3[0:0] == 1'b1) ? p_Result_3_fu_87_p4 : linear_value);

assign sext_ln703_1_fu_192_p1 = $signed(tmp_76_fu_185_p3);

assign sext_ln703_fu_181_p1 = $signed(tmp_75_fu_174_p3);

assign sub_ln1148_1_fu_161_p2 = (26'd0 - zext_ln1148_1_fu_154_p1);

assign sub_ln1148_fu_138_p2 = (72'd0 - t_fu_131_p3);

assign t_fu_131_p3 = {{select_ln180_reg_202}, {24'd0}};

assign tmp_75_fu_174_p3 = {{op_V_reg_222}, {4'd0}};

assign tmp_76_fu_185_p3 = {{op_V_reg_222}, {2'd0}};

assign tmp_fu_144_p4 = {{sub_ln1148_fu_138_p2[71:47]}};

assign x_V_fu_167_p3 = ((tmp_74_reg_207[0:0] == 1'b1) ? sub_ln1148_1_fu_161_p2 : zext_ln1148_fu_158_p1);

assign zext_ln1148_1_fu_154_p1 = tmp_fu_144_p4;

assign zext_ln1148_fu_158_p1 = trunc_ln1148_2_reg_212;

endmodule //dbfs_converter
