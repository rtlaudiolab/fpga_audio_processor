#ifndef __BIQUAD_EQUATION_HLS_HPP__
#define __BIQUAD_EQUATION_HLS_HPP__

double biquad_equation_hls( double i_a0,
                            double i_a1,
                            double i_a2,
                            double i_b1,
                            double i_b2,
                            double i_xn,
                            double i_xn_1,
                            double i_xn_2,
                            double i_yn_1,
                            double i_yn_2);

#endif
