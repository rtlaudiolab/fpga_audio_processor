#include "biquad_equation_hls.hpp"

double biquad_equation_hls(	double i_a0,
                            double i_a1,
                            double i_a2,
                            double i_b1,
                            double i_b2,
                            double i_xn,
                            double i_xn_1,
                            double i_xn_2,
                            double i_yn_1,
                            double i_yn_2) {

	double o_yn;
	o_yn = i_a0 * i_xn + i_a1 * i_xn_1 + i_a2 * i_xn_2 + i_b1 * i_yn_1 + i_b2 * i_yn_2;
	return o_yn;
}
