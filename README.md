# FPGA Audio Processor
The FPGA Audio Processor repository contains an FPGA audio processor design implemented on the ZedBoard. The repository stores the code described on the [weekly RTL Audio Lab blog](https://rtlaudiolab.com/Blog).

# Toolchain and Target Device
The FPGA Audio Processor project uses version 2021.2 of the Vivado HLx Editions design suite and targets the Zynq-7000 SoC XC7Z020-CLG484-1 device on the ZedBoard.

# Languages
The FPGA Audio Processor is mostly written in SystemVerilog, but it also includes C/C++ for high-level synthesis (HLS) and embedded software development (coming soon!).

 # Repository Structure
 The FPGA Audio Processor repository uses a simple structure with a single branch (main) for development. There is also a tag for each blog post in which files are added or modified, starting with post 13.