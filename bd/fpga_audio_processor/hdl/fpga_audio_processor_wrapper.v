//Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2021.2 (win64) Build 3367213 Tue Oct 19 02:48:09 MDT 2021
//Date        : Sun Mar 20 19:14:30 2022
//Host        : Isaac-Laptop running 64-bit major release  (build 9200)
//Command     : generate_target fpga_audio_processor_wrapper.bd
//Design      : fpga_audio_processor_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module fpga_audio_processor_wrapper
   (i_btnc,
    i_btnd,
    i_btnl,
    i_btnr,
    i_btnu,
    i_clock,
    i_codec_adc_data,
    i_codec_bit_clock,
    i_codec_lr_clock,
    i_spi_miso,
    i_sw0,
    i_sw1,
    i_sw2,
    i_sw3,
    i_sw4,
    i_sw5,
    i_sw6,
    i_sw7,
    o_codec_dac_data,
    o_codec_mclock,
    o_led,
    o_spi_clock,
    o_spi_cs_n,
    o_spi_mosi);
  input i_btnc;
  input i_btnd;
  input i_btnl;
  input i_btnr;
  input i_btnu;
  input i_clock;
  input i_codec_adc_data;
  input i_codec_bit_clock;
  input i_codec_lr_clock;
  input i_spi_miso;
  input i_sw0;
  input i_sw1;
  input i_sw2;
  input i_sw3;
  input i_sw4;
  input i_sw5;
  input i_sw6;
  input i_sw7;
  output o_codec_dac_data;
  output o_codec_mclock;
  output [7:0]o_led;
  output o_spi_clock;
  output o_spi_cs_n;
  output o_spi_mosi;

  wire i_btnc;
  wire i_btnd;
  wire i_btnl;
  wire i_btnr;
  wire i_btnu;
  wire i_clock;
  wire i_codec_adc_data;
  wire i_codec_bit_clock;
  wire i_codec_lr_clock;
  wire i_spi_miso;
  wire i_sw0;
  wire i_sw1;
  wire i_sw2;
  wire i_sw3;
  wire i_sw4;
  wire i_sw5;
  wire i_sw6;
  wire i_sw7;
  wire o_codec_dac_data;
  wire o_codec_mclock;
  wire [7:0]o_led;
  wire o_spi_clock;
  wire o_spi_cs_n;
  wire o_spi_mosi;

  fpga_audio_processor fpga_audio_processor_i
       (.i_btnc(i_btnc),
        .i_btnd(i_btnd),
        .i_btnl(i_btnl),
        .i_btnr(i_btnr),
        .i_btnu(i_btnu),
        .i_clock(i_clock),
        .i_codec_adc_data(i_codec_adc_data),
        .i_codec_bit_clock(i_codec_bit_clock),
        .i_codec_lr_clock(i_codec_lr_clock),
        .i_spi_miso(i_spi_miso),
        .i_sw0(i_sw0),
        .i_sw1(i_sw1),
        .i_sw2(i_sw2),
        .i_sw3(i_sw3),
        .i_sw4(i_sw4),
        .i_sw5(i_sw5),
        .i_sw6(i_sw6),
        .i_sw7(i_sw7),
        .o_codec_dac_data(o_codec_dac_data),
        .o_codec_mclock(o_codec_mclock),
        .o_led(o_led),
        .o_spi_clock(o_spi_clock),
        .o_spi_cs_n(o_spi_cs_n),
        .o_spi_mosi(o_spi_mosi));
endmodule
